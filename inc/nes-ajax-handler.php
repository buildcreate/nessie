<?php
//mimic the actuall admin-ajax
define('DOING_AJAX', true);

if (!isset( $_POST['action']))
    die('-1');

//make sure you update this line 
//to the relative location of the wp-load.php
require_once('../../../../wp-load.php'); 

//Typical headers
header('Content-Type: text/html');
send_nosniff_header();

//Disable caching
header('Cache-Control: no-cache');
header('Pragma: no-cache');

$action = esc_attr(trim($_POST['action']));

//A bit of security
$allowed_actions = array(
    'nes_ajax_vacancy_import',
    'nes_ajax_ecp_import',
    'nes_ajax_ical_import',
    'nes_ajax_get_venue_locations',
    'nes_ajax_view_change',
    'nes_ajax_make_event_form',
    'nes_ajax_get_gravity_form_fields',
    'nes_ajax_check_availability',
    'nes_ajax_get_time_select',
    'nes_ajax_attendee_check_in_out'
);

if(in_array($action, $allowed_actions)){
    if(is_user_logged_in()){
        do_action('NES_AJAX_HANDLER_'.$action);
    }else{
        do_action('NES_AJAX_HANDLER_nopriv_'.$action);
    }
}
else{
    die('-1');
} 