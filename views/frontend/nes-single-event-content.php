<?php 
    global $post;
    $content_area = $this->nes_settings['event_content_area_selector']; 
?> 
<div class="hideme" style="position:absolute;top:0;left:0;width:100%;height:100%;background:#fff;z-index:9999;"></div>
<div class="nes-single-event-content user-content" style="display:none;">
    <?php if(have_posts()) : ?>
        <?php while(have_posts()) : the_post(); ?>

        	<?php // get back to calendar setting ?>
        	<?php if($calendar_page_id = $this->nes_settings['event_calendar_page']) : ?>
	        	<a class="nes-back-button" href="<?php echo get_permalink($calendar_page_id); ?>"><i class="btr bt-angles-left"></i> Back to all <?php echo $this->nes_settings['event_plural']; ?></a>
	        <?php endif; ?>

        	<?php // check if this event is passed ?>
			<?php if(date('Y-m-d') > get_post_meta($post->ID,'nes_event_date',true)) : ?>
				<div class="nes-single-event-message">This <?php echo $this->nes_settings['event_single']; ?> has passed.</div>
			<?php endif; ?>

        	<h1 class="nes-event-title"><?php the_title(); ?></h1>
			<div class="nes-when-where">
				<div class="nes-when-where-info">
					<p><strong>When:</strong> <?php echo date('l - F jS, Y', strtotime(get_post_meta($post->ID, 'nes_event_date', true))); ?> @ <?php echo date('g:ia', strtotime(get_post_meta($post->ID, 'nes_start_time', true))); ?> - <?php echo date('g:ia', strtotime(get_post_meta($post->ID, 'nes_end_time', true))); ?></p>
					<?php 
						// check if onsite
						$event_type = get_post_meta($post->ID, 'nes_event_type', true);
						if($event_type == 'onsite'){
							// get venue
							$where = get_the_title(get_post_meta($post->ID, 'nes_venue_id', true));	
							// check for locations				
							$location_ids = get_post_meta($post->ID, 'nes_location_id', true);
							if($location_ids){
								$delimiter = ' - ';
								foreach($location_ids as $location_id){
									$where .= $delimiter . get_the_title($location_id);
									$delimiter = ', ';
								}
							} 
						}else{
							$where = get_post_meta($post->ID, 'nes_offsite_venue_name', true);
							if($address = get_post_meta($post->ID, 'nes_offsite_venue_address', true)){
								$where .= ' - <a target="_blank" href="http://maps.google.com/?q='.$address.'">'.$address.' <sup><i class="btr bt-external-link"></i></sup></a>';
							}
						}
						
					?>
					<?php if($where) : ?>
						<p><strong>Where:</strong> <?php echo $where; ?></p>
					<?php endif; ?>
					<?php
						// check if there is any contact info
						$contact_arr = array();
						if($name = get_post_meta($post->ID, 'nes_event_name', true)){$contact_arr['name'] = $name;}
						if($phone = get_post_meta($post->ID, 'nes_event_phone', true)){$contact_arr['phone'] = $phone;}
						if($email = get_post_meta($post->ID, 'nes_event_email', true)){$contact_arr['email'] = $email;}
						if($website = get_post_meta($post->ID, 'nes_event_website', true)){$contact_arr['website'] = $website;}
					?>
					<?php if(!empty($contact_arr)) : ?>
						<p><strong>Contact Info:</strong></p>
						<ul>
						<?php foreach($contact_arr as $k => $contact) : ?>
							<li>
								<?php if($k == 'phone') : ?>
									<a href="tel:<?php echo $contact; ?>"><?php echo $contact; ?></tel>
								<?php elseif($k == 'email') : ?>
									<address><a href="mailto:<?php echo $contact; ?>"><?php echo $contact; ?></a></address>
								<?php elseif($k == 'website') : ?>
									<a target="_blank" href="<?php echo $contact; ?>"><?php echo $contact; ?> <sup><i class="btr bt-external-link"></i></sup></a>
								<?php else : ?>
									<?php echo $contact; ?>
								<?php endif; ?>
							</li>
						<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
				<div class="nes-when-where-map">
					<?php if($event_type == 'onsite') : ?>
						<?php 
							// get venue address
							$address = $this->nes_get_venue_address($post->ID);
						?>
					<?php endif; ?>
					<div class="nes-embed-container">
						<iframe src="https://maps.google.com/maps?q=<?php echo $address; ?>&amp;output=embed"></iframe>
					</div>
					<div class="nes-buttons">
						<a target="_blank" class="nes-button" href="<?php echo $this->nes_google_calendar_export($post->ID); ?>"><i class="bts bt-plus"></i> GOOGLE CALENDAR</a>
						<a target="_blank" class="nes-button" href="<?php echo $this->nes_settings['dir'];?>views/frontend/nes-ical-feed.php?id=<?php echo $post->ID; ?>"><i class="bts bt-plus"></i> EXPORT ICAL EVENT</a>
					</div>
				</div>
				<span class="nes-clearer"></span>

			</div>

			<?php if($post->post_content) : ?>
				<div class="nes-event-content">
					<h2 class="nes-section-title">Event Description</h2>
					<?php the_content(); ?>
				</div>
			<?php endif; ?>

			<?php if(get_post_meta($post->ID,'nes_ticketed_event',true) == 'yes'): ?>
				<div class="nes-event-tickets">
					<?php if(get_post_meta($post->ID,'nes_event_date',true) >= date('Y-m-d')) : ?>
						<?php if(get_post_meta($post->ID,'nes_ticket_login',true) == 'no') : ?>
							<?php 
								// check if there are tickets available
								$limit = get_post_meta($post->ID,'nes_tickets_available',true);
								$attendees = get_post_meta($post->ID,'nes_attendees',true); 
								if(!$attendees){$attendees = array();}
								$nes_gravity_form = get_post_meta($post->ID,'nes_gravity_form',true);
							?>
							<?php if($limit) : ?>
								<?php if(count($attendees) >= $limit) : ?>
									<div class="nes-single-event-message">Sorry, tickets are sold out for this <?php echo $this->nes_settings['event_single']; ?>.</div>
								<?php else : ?>
									<div class="nes-single-event-message"><?php echo count($attendees); ?> of <?php echo $limit; ?> available tickets have been sold for this <?php echo $this->nes_settings['event_single']; ?>.</div>
									<?php gravity_form($nes_gravity_form, false, false, false, '', true); ?>
								<?php endif; ?>
							<?php else : ?>
								<?php gravity_form($nes_gravity_form, false, false, false, '', true); ?>
							<?php endif; ?>
						<?php else: ?>
							<?php if(is_user_logged_in()) : ?>
								<?php 
									// check if there are tickets available
									$limit = get_post_meta($post->ID,'nes_tickets_available',true); 
									$attendees = get_post_meta($post->ID,'nes_attendees',true);
									if(!$attendees){$attendees = array();}
									$nes_gravity_form = get_post_meta($post->ID,'nes_gravity_form',true);
								?>
								<?php if($limit) : ?>
									<?php if(count($attendees) >= $limit) : ?>
										<div class="nes-single-event-message">Sorry, tickets are sold out for this <?php echo $this->nes_settings['event_single']; ?>.</div>
									<?php else : ?>
										<div class="nes-single-event-message"><?php echo count($attendees); ?> of <?php echo $limit; ?> available tickets have been sold for this <?php echo $this->nes_settings['event_single']; ?>.</div>
										<?php gravity_form($nes_gravity_form, false, false, false, '', true); ?>
									<?php endif; ?>
								<?php else : ?>
									<?php gravity_form($nes_gravity_form, false, false, false, '', true); ?>
								<?php endif; ?>
							<?php else : ?>
								<div class="nes-single-event-message">You must be logged in to purchase tickets for this event.<a href="/wp-login.php?redirect_to=<?php echo get_permalink($post->ID); ?>">Please login here</a></div>
							<?php endif; ?>
						<?php endif; ?>
					<?php else : ?>
						<div class="nes-single-event-message">Sorry, this <?php echo $this->nes_settings['event_single']; ?> has passed and ticket sales are now closed.</div>
					<?php endif; ?>
					<span class="nes-clearer"></span>
				</div>
			<?php endif; ?>
			
		<?php endwhile; ?>
	<?php endif; ?>

	<?php
		global $wpdb;
	    $query_string = "
            SELECT DISTINCT p.ID
            FROM $wpdb->posts p
            LEFT JOIN $wpdb->postmeta m1 ON p.ID = m1.post_id
            LEFT JOIN $wpdb->postmeta m2 ON (p.ID = m2.post_id AND m2.meta_key = 'nes_event_date')
            LEFT JOIN $wpdb->postmeta m3 ON (p.ID = m3.post_id AND m3.meta_key = 'nes_start_time')
            WHERE p.post_type = 'nes_event'
            AND p.post_status IN ('publish', 'past_events')
            AND (m1.meta_key = 'nes_event_status' AND m1.meta_value = 'approved') 
            ORDER BY m2.meta_value DESC, m3.meta_value DESC
        ";
        $events = $wpdb->get_results($query_string, OBJECT);

		if($events){
			$prev_link = null;
			$prev_title = null;
			$prev_date = null;
			$next_link = null;
			$next_title = null;
			$next_date = null;
			foreach($events as $k => $event){

				// only get prev/next for current event
				if($post->ID == $event->ID){
					if($events[$k+1]){
						$prev_link = get_the_permalink($events[$k+1]->ID);
						$prev_title = get_the_title($events[$k+1]->ID);
						$prev_date = get_post_meta($events[$k+1]->ID, 'nes_event_date', true);
						$prev_start_time = get_post_meta($events[$k+1]->ID, 'nes_start_time', true);
					}

					if($events[$k-1]){
						$next_link = get_the_permalink($events[$k-1]->ID);
						$next_title = get_the_title($events[$k-1]->ID);
						$next_date = get_post_meta($events[$k-1]->ID, 'nes_event_date', true);
						$next_start_time = get_post_meta($events[$k-1]->ID, 'nes_start_time', true);
					}
				}
			}
		}
	?>
	<?php if($prev_link || $next_link) : ?>
		<div class="nes-event-pagination">
			<?php if($prev_link) : ?>
				<a class="nes-previous-event" href="<?php echo $prev_link; ?>"><i class="btr bt-angles-left"></i><span><strong><?php echo $prev_title; ?></strong><br/><?php echo date('F jS, Y', strtotime($prev_date)); ?> @ <?php echo date('g:ia', strtotime($prev_start_time)); ?></span></a>	
			<?php endif;?>	
			<?php if($next_link) : ?>
				<a class="nes-next-event" href="<?php echo $next_link; ?>"><i class="btr bt-angles-right"></i><span><strong><?php echo $next_title; ?></strong><br/><?php echo date('F jS, Y', strtotime($next_date)); ?> @ <?php echo date('g:ia', strtotime($next_start_time)); ?></span></a>
			<?php endif; ?>
			<span class="nes-clearer"></span>
		</div>
	<?php endif; ?>

</div>	
<script type="text/javascript">
    jQuery(document).ready(function($){
    	// remove from <head> and add to defined content area
    	$('<?php echo $content_area; ?>').html('');
    	$('.nes-single-event-content').detach().appendTo('<?php echo $content_area; ?>').removeAttr("style");
        $('.hideme').hide();
    });
</script>