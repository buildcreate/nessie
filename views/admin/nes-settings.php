<div class="nes-main-wrap">
	<h1 class="nes-page-title"><i class="icon-time"></i> <?php _e('Nessie Settings'); ?></h1>

	<?php // show notice for saving settings ?>
	<?php if(!empty($_POST['nes_update_settings'])) : ?>
		<div class="updated">
		    <p><strong><?php _e('SUCCESS!','nes'); ?></strong> <?php _e('Your settings have been saved.','nes'); ?></p>
		</div>
	<?php endif; ?>

	<div id="nes-tabs">
		<h2 class="nav-tab-wrapper">
			<?php $active_tab = isset($_GET['tab']) ? $_GET['tab'] : 'nes-general'; ?>
			<a href="?page=nes-settings&tab=nes-general" class="nav-tab <?php echo $active_tab == 'nes-general' ? 'nav-tab-active' : ''; ?>"><?php _e('General','nes');?></a>
			<a href="?page=nes-settings&tab=nes-labels" class="nav-tab <?php echo $active_tab == 'nes-labels' ? 'nav-tab-active' : ''; ?>"><?php _e('Labels','nes');?></a>			
			<a href="?page=nes-settings&tab=nes-layout-styles" class="nav-tab <?php echo $active_tab == 'nes-layout-styles' ? 'nav-tab-active' : ''; ?>"><?php _e('Layout & Styles','nes');?></a>
			<a href="?page=nes-settings&tab=nes-forms" class="nav-tab <?php echo $active_tab == 'nes-forms' ? 'nav-tab-active' : ''; ?>"><?php _e('Forms','nes');?></a>
			<a href="?page=nes-settings&tab=nes-notifications" class="nav-tab <?php echo $active_tab == 'nes-notifications' ? 'nav-tab-active' : ''; ?>"><?php _e('Notifications','nes');?></a>
			<a href="?page=nes-settings&tab=nes-setup-usage" class="nav-tab <?php echo $active_tab == 'nes-setup-usage' ? 'nav-tab-active' : ''; ?>"><?php _e('Setup & Usage','nes');?></a>
			<span id="nes-shortcode"><?php _e('Nessie shortcode','nes');?> <code>[nessie view="month|list|reservations"]</code></span>
			<span class="nes-clearer"></span>
		</h2>
	</div>
	<div id="nes-settings-wrap">
		<?php if($active_tab == 'nes-general') : ?>
			<div id="nes-general">
				<form method="post" action="">
					<h2 class="nes-tab-title"><?php _e('General Settings','nes'); ?></h2><hr/>
					<p><?php _e('Here you can customize the general Nessie settings to your liking','nes'); ?>.</p>
					<br/>
					<p>
						<?php printf(__('Selector for %1$s content area','nes'),$this->nes_settings['event_single']); ?><br/>
						<input type="text" name="nes_event_content_area_selector" value="<?php echo $this->nes_settings['event_content_area_selector']; ?>"/>
					</p>	

					<?php // get all site pages ?>				
					<?php $pages = get_posts('post_type=page&posts_per_page=-1'); ?>
					<?php if($pages) : ?>
						<div class="nes-event-page-settings">
							<div>
								<label>Which page is your <code>[nessie view="month|list"]</code> shortcode on?</label>
								<select name="nes_calendar_page">
									<option value="">- Choose a Page -</option>
									<?php foreach($pages as $page) : ?>
										<option value="<?php echo $page->ID; ?>" <?php if($this->nes_settings['event_calendar_page'] == $page->ID){echo 'selected="selected"';} ?>><?php echo $page->post_title; ?></option>
									<?php endforeach; ?>
								</select>
							</div>					
							<div>
								<label>Which page is your <code>[nessie view="reservations"]</code> shortcode on?</label>
								<select name="nes_reservation_page">
									<option value="">- Choose a Page -</option>
									<?php foreach($pages as $page) : ?>
										<option value="<?php echo $page->ID; ?>" <?php if($this->nes_settings['event_reservation_page'] == $page->ID){echo 'selected="selected"';} ?>><?php echo $page->post_title; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						<?php endif; ?>	

							<div>
								<label>Show Reservation Link on <?php $this->nes_settings['event_plural']; ?> Calendar</label>
								<select name="nes_show_reservation_link">
									<option value="yes" <?php if($this->nes_settings['show_reservation_link'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
									<option value="no" <?php if($this->nes_settings['show_reservation_link'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
								</select>
							</div>							
							<div>
								<label>Max <?php $this->nes_settings['event_plural']; ?> until "view more" on Calendar</label>
								<input type="number" name="nes_max_events_per_day" value="<?php echo $this->nes_settings['max_events_per_day']; ?>" />
							</div>		
							<div>
								<label>iCal Feed title:</label>
								<input type="text" name="nes_ical_feed_title" value="<?php echo get_option('nes_ical_feed_title'); ?>" />
							</div>
						</div>
					
					<p>
						<label><?php printf(__('Show %1$s Details','nes'),$this->nes_settings['event_single']); ?></label>
						<select name="nes_show_event_details">
							<option value="yes" <?php if($this->nes_settings['show_event_details'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['show_event_details'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
						</select>
					</p>					
					<p>
						<label><?php _e('Require Login','nes'); ?></label>
						<select name="nes_require_login">
							<option value="yes" <?php if($this->nes_settings['require_login'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['require_login'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
						</select>
					</p>
					<p>
						<label><?php _e('Hide WP admin bar','nes'); ?></label>
						<select name="nes_hide_admin_bar">
							<option value="yes" <?php if($this->nes_settings['hide_admin_bar'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['hide_admin_bar'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
						</select>
						<div id="nes-show-admin-bar" style="display:none;">
							<p><?php _e('BUT, Still Show WP admin bar for','nes'); ?></p>
							<?php global $wp_roles; ?>
							<ul>
							<?php foreach($wp_roles->roles as $role => $data) : ?>
								<li>
									<input id="<?php echo $role; ?>" type="checkbox" name="nes_show_admin_bar_for[]" value="<?php echo $role; ?>" <?php if(is_array($this->nes_settings['show_admin_bar_for'])){if(in_array($role, $this->nes_settings['show_admin_bar_for'])){echo 'checked';}}else if($role == $this->nes_settings['show_admin_bar_for']){echo 'checked';} ?>/> <label for="<?php echo $role; ?>"><?php echo $role; ?></label>
									<span class="nes-clearer"></span>
								</li>
							<?php endforeach; ?>
							</ul>
						</div>
					</p>
					<p>
						<label><?php printf(__('Default %1$s','nes'),$this->nes_settings['venue_single']); ?></label>
						<?php $venues = $this->nes_get_venues(); ?>
						<?php if($venues) : ?>
						<select id="nes-venue-id" name="nes_default_venue">
							<?php foreach($venues as $venue) : ?>
								<option value="<?php echo $venue->ID; ?>" <?php if(get_option('nes_default_venue') == $venue->ID){echo 'selected';} ?>><?php echo get_the_title($venue->ID); ?></option>
							<?php endforeach; ?>
						</select>
						<?php else : ?>
							<p><?php printf(__('No %1$s have been created yet','nes'),$this->nes_settings['venue_plural']); ?>. <a href="/wp-admin/post-new.php?post_type=nes_venue"><?php printf(__('Create new %1$s','nes'),$this->nes_settings['venue_single']); ?></a>
						<?php endif; ?>
					</p>
					<p>
						<label><?php _e('Day View Start Time','nes'); ?></label>
						<?php echo $this->nes_get_time_select('nes_day_start_time',$this->nes_settings['day_start_time']); ?>
					</p>
					<p>
						<label><?php _e('Day View End Time','nes'); ?></label>
						<?php echo $this->nes_get_time_select('nes_day_end_time',$this->nes_settings['day_end_time']); ?>
					</p>
					<p>
						<label><?php _e('Use Setup/Cleanup Times','nes'); ?>?</label>
						<select name="nes_setup_cleanup">
							<option value="yes" <?php if($this->nes_settings['setup_cleanup'] == 'yes'){echo 'selected';}?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['setup_cleanup'] == 'no'){echo 'selected';}?>><?php _e('No','nes'); ?></option>
						</select>

					</p>
					<p>
						<?php printf(__('Message to display after successful %1$s submission','nes'),$this->nes_settings['event_single']); ?><br/>
						<input type="text" name="nes_event_success_message" size="80" value="<?php echo $this->nes_settings['event_success_message']; ?>"/>
					</p>
					<input class="button button-primary" type="submit" name="nes_update_settings" value="Update Settings" />
				</form>
			</div>
		<?php elseif($active_tab == 'nes-forms') : ?>
			<div id="nes-forms">
				<form method="post" action="">
					<h2 class="nes-tab-title"><?php printf(__('%1$s End Time Options','nes'),$this->nes_settings['event_single']); ?></h2><hr/>
					<div id="nes-end-time-options">
						<div class="event-end-time-type">
							<p>
								<label style="width:auto;"><input id="nes-standard-end-time" type="radio" name="nes_end_time_type" value="standard" <?php if($this->nes_settings['end_time_type'] == 'standard'){echo 'checked';};?> /> <?php _e('Standard End Time Selection (any length)','nes'); ?></label><br/>
								<label style="width:auto;"><input id="nes-fixed-end-time" type="radio" name="nes_end_time_type" value="fixed" <?php if($this->nes_settings['end_time_type'] == 'fixed'){echo 'checked';};?> /> <?php printf(__('Fixed %1$s Length','nes'), $this->nes_settings['event_single']); ?></label><br/>
								<label style="width:auto;"><input id="nes-minmax-end-time" type="radio" name="nes_end_time_type" value="minmax" <?php if($this->nes_settings['end_time_type'] == 'minmax'){echo 'checked';};?> /> <?php printf(__('Min/Max %1$s Length Selection','nes'),$this->nes_settings['event_single']); ?></label>
							</p>
						</div>
						<span class="nes-clearer"></span>
						<div class="event-fixed-end-times" style="display:none;">
							<p>
								<?php printf(__('%1$s last for','nes'),$this->nes_settings['event_plural']); ?>:<br/>
								<select name="nes_end_time_length_hr">
								<?php for($i=0;$i<24;$i++) : ?>
									<option value="<?php echo $i; ?>" <?php if($this->nes_settings['end_time_length_hr'] == $i){echo 'selected';};?>><?php echo $i; ?></option>
								<?php endfor; ?>
								</select> <?php _e('hours','nes'); ?> 
								<select name="nes_end_time_length_min">
									<option value="00" <?php if($this->nes_settings['end_time_length_min'] == '00'){echo 'selected';};?>>00</option>
									<option value="15" <?php if($this->nes_settings['end_time_length_min'] == '15'){echo 'selected';};?>>15</option>
									<option value="30" <?php if($this->nes_settings['end_time_length_min'] == '30'){echo 'selected';};?>>30</option>
									<option value="45" <?php if($this->nes_settings['end_time_length_min'] == '45'){echo 'selected';};?>>45</option>
								</select> <?php _e('minutes','nes'); ?>
							</p>
						</div>	
						<span class="nes-clearer"></span>				
						<div class="event-minmax-end-times" style="display:none;">
							<p>
								<?php printf(__('Minimum %1$s Length','nes'),$this->nes_settings['event_single']); ?>:<br/><span class="description">(<?php _e('if a user chooses a 1:00pm start time and this setting is 30 minutes, the first available end time will be 1:30pm','nes');?>)</span><br/>
								<select name="nes_end_time_min_length_hr">
								<?php for($i=0;$i<24;$i++) : ?>
									<option value="<?php echo $i; ?>" <?php if($this->nes_settings['end_time_min_length_hr'] == $i){echo 'selected';};?>><?php echo $i; ?></option>
								<?php endfor; ?>
								</select> <?php _e('hours','nes'); ?>
								<select name="nes_end_time_min_length_min">
									<option value="00" <?php if($this->nes_settings['end_time_min_length_min'] == '00'){echo 'selected';};?>>00</option>
									<option value="15" <?php if($this->nes_settings['end_time_min_length_min'] == '15'){echo 'selected';};?>>15</option>
									<option value="30" <?php if($this->nes_settings['end_time_min_length_min'] == '30'){echo 'selected';};?>>30</option>
									<option value="45" <?php if($this->nes_settings['end_time_min_length_min'] == '45'){echo 'selected';};?>>45</option>
								</select> <?php _e('minutes','nes'); ?>
							</p>							
							<p>
								<?php printf(__('Maximum %1$s Length','nes'),$this->nes_settings['event_single']); ?>:<br/><span class="description">(<?php _e('if a user chooses a 1:00pm start time and this setting is 2 hours, the last available end time will be 3:00pm','nes'); ?>)</span><br/>
								<select name="nes_end_time_max_length_hr">
								<?php for($i=0;$i<24;$i++) : ?>
									<option value="<?php echo $i; ?>" <?php if($this->nes_settings['end_time_max_length_hr'] == $i){echo 'selected';};?>><?php echo $i; ?></option>
								<?php endfor; ?>
								</select> <?php _e('hours','nes'); ?> 
								<select name="nes_end_time_max_length_min">
									<option value="00" <?php if($this->nes_settings['end_time_max_length_min'] == '00'){echo 'selected';};?>>00</option>
									<option value="15" <?php if($this->nes_settings['end_time_max_length_min'] == '15'){echo 'selected';};?>>15</option>
									<option value="30" <?php if($this->nes_settings['end_time_max_length_min'] == '30'){echo 'selected';};?>>30</option>
									<option value="45" <?php if($this->nes_settings['end_time_max_length_min'] == '45'){echo 'selected';};?>>45</option>
								</select> <?php _e('minutes','nes'); ?>
							</p>
							<p>								
								<?php printf(__('%1$s Time Interval','nes'),$this->nes_settings['event_single']); ?>:<br/><span class="description">(<?php _e('end times available to the user will be in intervals of this setting. make sure it fits evenly between your Min/Max settings above','nes'); ?>)</span><br/>
								<select name="nes_end_time_minmax_interval">
									<option value="0.25" <?php if($this->nes_settings['end_time_minmax_interval'] == '0.25'){echo 'selected';};?>><?php _e('15 minutes','nes'); ?></option>
									<option value="0.5" <?php if($this->nes_settings['end_time_minmax_interval'] == '0.5'){echo 'selected';};?>><?php _e('30 minutes','nes'); ?></option>
									<option value="0.75" <?php if($this->nes_settings['end_time_minmax_interval'] == '0.75'){echo 'selected';};?>><?php _e('45 minutes','nes'); ?></option>
									<option value="1" <?php if($this->nes_settings['end_time_minmax_interval'] == '1'){echo 'selected';};?>><?php _e('1 hour','nes'); ?></option>
									<option value="1.25" <?php if($this->nes_settings['end_time_minmax_interval'] == '1.25'){echo 'selected';};?>><?php _e('1 hour 15 minutes','nes'); ?></option>
									<option value="1.5" <?php if($this->nes_settings['end_time_minmax_interval'] == '1.5'){echo 'selected';};?>><?php _e('1 hour 30 minutes','nes'); ?></option>
									<option value="1.75" <?php if($this->nes_settings['end_time_minmax_interval'] == '1.75'){echo 'selected';};?>><?php _e('1 hour 45 minutes','nes'); ?></option>
									<option value="2" <?php if($this->nes_settings['end_time_minmax_interval'] == '2'){echo 'selected';};?>><?php _e('2 hours','nes'); ?></option>
									<option value="2.25" <?php if($this->nes_settings['end_time_minmax_interval'] == '2.25'){echo 'selected';};?>><?php _e('2 hours 15 minutes','nes'); ?></option>
									<option value="2.5" <?php if($this->nes_settings['end_time_minmax_interval'] == '2.5'){echo 'selected';};?>><?php _e('2 hours 30 minutes','nes'); ?></option>
									<option value="2.75" <?php if($this->nes_settings['end_time_minmax_interval'] == '2.75'){echo 'selected';};?>><?php _e('2 hours 45 minutes','nes'); ?></option>
									<option value="3" <?php if($this->nes_settings['end_time_minmax_interval'] == '3'){echo 'selected';};?>><?php _e('3 hours','nes'); ?></option>
									<option value="3.25" <?php if($this->nes_settings['end_time_minmax_interval'] == '3.25'){echo 'selected';};?>><?php _e('3 hours 15 minutes','nes'); ?></option>
									<option value="3.5" <?php if($this->nes_settings['end_time_minmax_interval'] == '3.5'){echo 'selected';};?>><?php _e('3 hours 30 minutes','nes'); ?></option>
									<option value="3.75" <?php if($this->nes_settings['end_time_minmax_interval'] == '3.75'){echo 'selected';};?>><?php _e('3 hours 45 minutes','nes'); ?></option>
									<option value="4" <?php if($this->nes_settings['end_time_minmax_interval'] == '4'){echo 'selected';};?>><?php _e('4 hours','nes'); ?></option>
									<option value="4.25" <?php if($this->nes_settings['end_time_minmax_interval'] == '4.25'){echo 'selected';};?>><?php _e('4 hours 15 minutes','nes'); ?></option>
									<option value="4.5" <?php if($this->nes_settings['end_time_minmax_interval'] == '4.5'){echo 'selected';};?>><?php _e('4 hours 30 minutes','nes'); ?></option>
									<option value="4.75" <?php if($this->nes_settings['end_time_minmax_interval'] == '4.75'){echo 'selected';};?>><?php _e('4 hours 45 minutes','nes'); ?></option>
									<option value="5" <?php if($this->nes_settings['end_time_minmax_interval'] == '5'){echo 'selected';};?>><?php _e('5 hours','nes'); ?></option>
								</select>
							</p>
							<p>
								<?php _e('Match Interval to Calendar Intervals','nes'); ?><br/>
								<select name="nes_match_minmax_interval">
									<option value="no" <?php if($this->nes_settings['match_minmax_interval'] == 'no'){echo 'selected';};?>><?php _e('No','nes'); ?></option>
									<option value="yes" <?php if($this->nes_settings['match_minmax_interval'] == 'yes'){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
								</select>
							</p>
						</div>
					</div>
					<br/>
					<h2 class="nes-tab-title"><?php _e('Form Field Visibility','nes'); ?></h2><hr/>
					<div id="nes-show-form-fields">
						<p>
							<?php printf(__('Display these fields on the %1$s form','nes'),$this->nes_settings['event_single']); ?>:<br/>
							<span class="description">(<?php _e('Name, Email, Date, Start Time & End Time are always required and cannot be hidden','nes'); ?>)</span>
						</p>
						
						<ul>
						<?php
							if(!empty($this->nes_settings['title_label'])){
								$title = $this->nes_settings['title_label'];
							}else{
								$title = sprintf(__('%1$s Title','nes'),$this->nes_settings['event_single']);
							}							
							if(!empty($this->nes_settings['website_label'])){
								$website = $this->nes_settings['website_label'];
							}else{
								$website = __('Website','nes');
							}							
							if(!empty($this->nes_settings['event_type_label'])){
								$type = $this->nes_settings['event_type_label'];
							}else{
								$type = sprintf(__('%1$s Type','nes'),$this->nes_settings['event_single']);
							}							
							if(!empty($this->nes_settings['description_label'])){
								$description = $this->nes_settings['description_label'];
							}else{
								$description = sprintf(__('%1$s Description','nes'),$this->nes_settings['event_single']);
							}							
							if(!empty($this->nes_settings['setup_needs_label'])){
								$setup = $this->nes_settings['setup_needs_label'];
							}else{
								$setup = __('Setup Needs','nes');
							}					
							if(!empty($this->nes_settings['av_needs_label'])){
								$av = $this->nes_settings['av_needs_label'];
							}else{
								$av = __('A/V Tech Needs: (ie. Screen, Projector, Speakers, Microphone, etc.)','nes');
							}
							$fields = array(
								//'end_time' => 'End Time',
								'setup_time' => __('Setup Time','nes'),
								'cleanup_time' => __('Cleanup Time','nes'),
								'title' => $title,
								'venue' => $this->nes_settings['venue_single'],
								'location' => $this->nes_settings['location_single'],
								'website' => $website,
								'type' => $type,
								'description' => $description,
								'setup_needs' => $setup,
								'av_needs' => $av
							); 
						?>
						<?php foreach($fields as $key => $value) : ?>
							<li>
								<input id="<?php echo $key; ?>" type="checkbox" name="nes_show_form_fields[]" value="<?php echo $key; ?>" <?php if(is_array($this->nes_settings['show_form_fields'])){if(in_array($key, $this->nes_settings['show_form_fields'])){echo 'checked';}}else if($key == $this->nes_settings['show_form_fields']){echo 'checked';} ?>/> <label for="<?php echo $key; ?>"><?php echo $value; ?></label>
								<span class="nes-clearer"></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</div>
					<br/>
					<h2 class="nes-tab-title"><?php _e('Form Field Labels','nes'); ?></h2><hr/>
					<p><?php _e('This allows you to update the labels on the event form','nes'); ?>. <?php _e('Leave blank for default values shown','nes'); ?>.</p>
					<div class="form-field-labels">
						<p>
							<label><?php printf(__('%1$s Title','nes'),$this->nes_settings['event_single']); ?>:</label>
							<input type="text" name="nes_title_label" value="<?php echo $this->nes_settings['title_label'];?>"/>
						</p>					
						<p>
							<label><?php _e('Your Name','nes'); ?>:</label>
							<input type="text" name="nes_name_label" value="<?php echo $this->nes_settings['name_label'];?>"/>
						</p>
						<p>
							<label><?php _e('Phone','nes'); ?>:</label>
							<input type="text" name="nes_phone_label" value="<?php echo $this->nes_settings['phone_label']; ?>"/>
						</p>					
						<p>
							<label><?php _e('Email','nes'); ?>:</label>
							<input type="text" name="nes_email_label" value="<?php echo $this->nes_settings['email_label'];?>"/>
						</p>						
						<p>
							<label><?php _e('Website','nes'); ?>:</label>
							<input type="text" name="nes_website_label" value="<?php echo $this->nes_settings['website_label'];?>"/>
						</p>
						<p>
							<label><?php printf(__('%1$s Type','nes'),$this->nes_settings['event_single']); ?>:</label>
							<input type="text" name="nes_event_type_label" value="<?php echo $this->nes_settings['event_type_label'];?>"/>
						</p>					
						<p>
							<label><?php printf(__('%1$s Description'),$this->nes_settings['event_single']); ?>:</label>
							<input type="text" name="nes_description_label" value="<?php echo $this->nes_settings['description_label'];?>"/>
						</p>					
						<p>
							<label><?php _e('Setup Needs','nes'); ?>:</label>
							<input type="text" name="nes_setup_needs_label" value="<?php echo $this->nes_settings['setup_needs_label'];?>"/>
						</p>					
						<p>
							<label><?php _e('A/V Tech Needs: (ie. Screen, Projector, Speakers, Microphone, etc.)','nes'); ?></label>
							<input type="text" name="nes_av_needs_label" value="<?php echo $this->nes_settings['av_needs_label'];?>"/>
						</p>
					</div>
					<br/>
					<input type="hidden" name="nes_save_form_settings" value="1" />
					<input class="button button-primary" type="submit" name="nes_update_settings" value="Update Settings" />
				</form>
			</div>
		<?php elseif($active_tab == 'nes-notifications') : ?>
			<div id="nes-notifications">
				<form method="post" action="">
					<h2 class="nes-tab-title"><?php _e('Notification Settings','nes'); ?></h2><hr/>
					<p><?php _e('These notification settings are for the emails that Nessie can send when various actions occur','nes'); ?>. <?php _e('Customize these to your liking','nes'); ?>.</p>
					<h3><?php _e('Choose when Notifications are sent','nes'); ?>:</h3>
					<p>
						<strong><?php printf(__('Send admin notification when a new %1$s is submitted','nes'),$this->nes_settings['event_single']); ?></strong> &nbsp;
						<select name="nes_admin_new_notification">
							<option value="yes" <?php if($this->nes_settings['admin_new_notification'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['admin_new_notification'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
						</select>
					</p>
					<p>
						<strong><?php printf(__('Send user notification when a new %1$s is submitted','nes'),$this->nes_settings['event_single']); ?></strong> &nbsp;
						<select name="nes_user_new_notification">
							<option value="yes" <?php if($this->nes_settings['user_new_notification'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['user_new_notification'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
						</select>
					</p>	
					<p>
						<strong><?php printf(__('Send user notification when a %1$s is approved/denied','nes'),$this->nes_settings['event_single']); ?></strong> &nbsp;
						<select name="nes_user_approved_notification">
							<option value="yes" <?php if($this->nes_settings['user_approved_notification'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
							<option value="no" <?php if($this->nes_settings['user_approved_notification'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
						</select>
					</p>
					<h3><?php _e('This is where your Notifications are from','nes'); ?>:</h3>
					<p>
						<strong><?php _e('FROM Email name','nes'); ?></strong><br/>
						<input type="text" name="nes_from_email_name" size="80" value="<?php echo $this->nes_settings['from_email_name']; ?>"/>
						<br/><span class="description"><?php _e('the name to appear as who notifications are from','nes'); ?></span>
					</p>
					<p>
						<strong><?php _e('FROM Email address','nes'); ?></strong><br/>
						<input type="text" name="nes_from_email_address" size="80" value="<?php echo $this->nes_settings['from_email_address']; ?>"/>
						<br/><span class="description"><?php _e('this should match your domain to help avoid spam filtering','nes'); ?></span>
					</p>	
					<h3><?php _e('These are where Admin Notifications can go to','nes'); ?>:</h3>
					<p><?php printf(__('This will be chosen by the user when submitting a %1$s','nes'),$this->nes_settings['event_single']); ?>. <?php _e('Leave the second label and email address blank if you only want to give the user one option','nes'); ?>.</p>
					<table>
					<tr>
						<td>
							<p id="nes-admin-email-label-one">
								<strong><?php _e('Label','nes'); ?></strong><br/>
								<input type="text" name="nes_admin_email_label_one" size="20" value="<?php echo $this->nes_settings['admin_email_label_one']; ?>"/>
								<br/><span class="description"> (<?php _e('i.e. Rentals','nes'); ?>)</span>
							</p>	
						</td>
						<td>
							<p id="nes-admin-email-one">
								<strong><?php _e('TO email address(es)','nes'); ?></strong><br/>
								<input type="text" name="nes_admin_email_one" size="54" value="<?php echo $this->nes_settings['admin_email_one']; ?>"/>
								<br/><span class="description"><?php _e('comma separate multiple emails','nes'); ?></span>
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<p id="nes-admin-email-label-two">
								<strong><?php _e('Label','nes'); ?></strong><br/>
								<input type="text" name="nes_admin_email_label_two" size="20" value="<?php echo $this->nes_settings['admin_email_label_two']; ?>"/>
								<br/><span class="description"> (<?php _e('i.e. Student Groups','nes'); ?>)</span>
							</p>	
						</td>
						<td>
							<p id="nes-admin-email-two">
								<strong><?php _e('TO email address(es)','nes'); ?></strong><br/>
								<input type="text" name="nes_admin_email_two" size="54" value="<?php echo $this->nes_settings['admin_email_two']; ?>"/>
								<br/><span class="description"><?php _e('comma separate multiple emails','nes'); ?></span>
							</p>
						</td>
					</tr>
					</table>
					<h3><?php _e('This is the content of your Notifications','nes'); ?>:</h3>
					<p>
						<strong><?php printf(__('User notification for new %1$s subject line','nes'),$this->nes_settings['event_single']); ?></strong><br/>
						<input type="text" name="nes_user_subject_line_new" size="80" value="<?php echo $this->nes_settings['user_subject_line_new']; ?>"/>
						<br/><span class="description"><?php _e('leave blank for default','nes'); ?></span>
					</p>
					<p>
						<strong><?php printf(__('User notification for approved/denied %1$s subject line','nes'),$this->nes_settings['event_single']); ?></strong><br/>
						<input type="text" name="nes_user_subject_line_approved" size="80" value="<?php echo $this->nes_settings['user_subject_line_approved']; ?>"/>
						<br/><span class="description"><?php _e('leave blank for default','nes'); ?></span>
					</p>

					<h3><?php _e('This is the Header/Footer for all Notifications','nes'); ?>:</h3>
					<p>
						<strong><?php _e('Email Notification Header','nes'); ?></strong><br/>
						<textarea name="nes_notification_header" rows="8" cols="80"><?php echo get_option('nes_notification_header'); ?></textarea>
						<br/><span class="description"><?php _e('leave blank for default','nes'); ?></span>
					</p>
					<p>
						<strong><?php _e('Email Notification Footer','nes'); ?></strong><br/>
						<textarea name="nes_notification_footer" rows="8" cols="80"><?php echo get_option('nes_notification_footer'); ?></textarea>
						<br/><span class="description"><?php _e('Organization contact info will generally go here','nes'); ?></span>
					</p>
					<input class="button button-primary" type="submit" name="nes_update_settings" value="Update Settings" />
				</form>
			</div>
		<?php elseif($active_tab == 'nes-labels') : ?>
			<div id="nes-labels">
				<form method="post" action="">
					<h2 class="nes-tab-title"><?php _e('Update Labels','nes'); ?></h2><hr/>
					<p><?php _e('This allows you to update all the labels of the content types of Nessie','nes'); ?>. <?php _e('We recommended choosing labels that will better clarify your situation','nes'); ?>. <?php _e('Examples for "Venue/Location/Event" might be "Restaurant/Table/Event" or "Building/Room/Appointment"','nes'); ?>.</p>
					<br/>
					<p>
						<label><?php _e('Single Venue label','nes'); ?></label>
						<input type="text" name="nes_venue_single" value="<?php echo $this->nes_settings['venue_single']; ?>"/>
					</p>
					<p>
						<label><?php _e('Plural Venue label','nes'); ?></label>
						<input type="text" name="nes_venue_plural" value="<?php echo $this->nes_settings['venue_plural'];?>"/>
					</p>
					<p>
						<label><?php _e('Single Location label','nes'); ?></label>
						<input type="text" name="nes_location_single" value="<?php echo $this->nes_settings['location_single']; ?>"/>
					</p>
					<p>
						<label><?php _e('Plural Location label','nes'); ?></label>
						<input type="text" name="nes_location_plural" value="<?php echo $this->nes_settings['location_plural'];?>"/>
					</p>
					<p>
						<label><?php _e('Single Event label','nes'); ?></label>
						<input type="text" name="nes_event_single" value="<?php echo $this->nes_settings['event_single']; ?>"/>
					</p>
					<p>
						<label><?php _e('Plural Event label','nes'); ?></label>
						<input type="text" name="nes_event_plural" value="<?php echo $this->nes_settings['event_plural'];?>"/>
					</p>
					<input class="button button-primary" type="submit" name="nes_update_settings" value="Update Settings" />
				</form>
			</div>		
		<?php elseif($active_tab == 'nes-setup-usage') : ?>
			<?php // remove setup nag if notice was clicked ?>
			<?php if(isset($_GET['notice']) && $_GET['notice'] == '1'){update_option('nes_setup_usage', 1);}?>
			<div id="nes-setup-usage">
				<h2 class="nes-tab-title"><?php _e('Setting up and Using Nessie','nes'); ?></h2><hr/>
				<p><?php _e('Nessie is a event system that is comprised of 3 interacting content types','nes'); ?>:</p?>
				<br/>
				<h3><?php _e('The Basics','nes'); ?></h3>
				<ul>
					<li><strong><?php echo $this->nes_settings['venue_plural']; ?></strong> - <?php printf(__('These are the overarching places your %1$s will be assigned to','nes'),$this->nes_settings['location_plural']); ?>. <?php _e('An example might be "The Chamber of Commerce"','nes'); ?>.</li>
					<li><strong><?php echo $this->nes_settings['location_plural']; ?></strong> - <?php printf(__('These are sections of a %1$s will be assigned to','nes'),$this->nes_settings['venue_single']); ?>. <?php _e('An example might be "Conference Room"','nes'); ?>.</li>
					<li><strong><?php echo $this->nes_settings['event_plural']; ?></strong> - <?php _e('These are what your users will be creating when they make a event','nes'); ?>. <?php printf(__('Each %1$s will be assigned to one or more %2$s','nes'),$this->nes_settings['event_single'],$this->nes_settings['location_plural']); ?>.</li>
				</ul>
				<h3><?php _e('Initial Setup','nes'); ?></h3>
				<ul>
					<li><?php printf(__('The first thing you\'ll need to do is %1$screate a %2$s','nes'),'<a target="_blank" href="/wp-admin/edit.php?post_type=nes_venue">',$this->nes_settings['venue_single']); ?></a>. <?php printf(__('This is done by clicking on the "%1$s" submenu item under "Nessie" in the Wordpress admin sidebar and then adding a new post','nes'),$this->nes_settings['venue_plural']); ?>. <?php printf(__('Give your %1$s a title and complete the meta fields','nes'),$this->nes_settings['venue_single']); ?>.</li>
					<li><?php printf(__('Next you\'ll need to %1$screate a %2$s','nes'),'<a target="_blank" href="/wp-admin/edit.php?post_type=nes_location">',$this->nes_settings['location_single']); ?></a>. <?php printf(__('This is done by clicking on the "%1$s" submenu item under "Nessie" in the Wordpress admin sidebar and then adding a new post','nes'),$this->nes_settings['location_plural']); ?>. <?php printf(__('Give your %1$s a title and complete the meta fields','nes'),$this->nes_settings['location_single']); ?>. <?php printf(__('Each %1$s can use the availability of the %2$s you assign it to, or can use its own availability','nes'),$this->nes_settings['location_single'],$this->nes_settings['venue_single']); ?>.</li>
					<li><?php printf(__('Once you\'ve created your %1$s & %2$s you should visit the "General", "Labels", "Forms" and "Notifications" tabs above','nes'),$this->nes_settings['venue_plural'],$this->nes_settings['location_plural']); ?>. <?php _e('Browse through the settings and configure Nessie to behave the way you want it to','nes'); ?>.</li>
				</ul>
				<h3><?php _e('Usage','nes'); ?></h3>
				<ul>
					<li><?php _e('Nessie is displayed to the users of your site through a shortcode','nes'); ?>. <?php printf(__('Simply put the shortcode %1$s in any page or post and enjoy. Use the "view" parameter to show either the "Month" view, the "List" view, or the "Reservation" view.','nes'),'<code>[nessie view="month|list|reservation"]</code>'); ?>!</li>
					<li><?php printf(__('Users will submit %1$s requests from the Nessie interface generated on the page that you placed the shortcode','nes'),$this->nes_settings['event_single']); ?>.</li>
					<li><?php _e('Submitted requests will be created with a "Pending" status','nes'); ?>. <?php printf(__('When viewing the %1$s request you can change the its status to "Approved" or "Denied"','nes'),$this->nes_settings['event_single']); ?>. <?php printf(__('This will help keep your %1$s system organized and also keep the user informed by sending out notifications of the status change (if configured in the %2$sNotifications settings tab%3$s','nes'),$this->nes_settings['event_single'],'<a href="/wp-admin/admin.php?page=nes-settings&tab=nes-notifications">','</a>)'); ?>.</li>
					<li><?php printf(__('If you ever need to reorder the way your %1$s display on the front end you can simply set their "menu order" page attribute to order them as you wish','nes'),$this->nes_settings['location_plural']); ?>.</li>
				</ul>
			</div>
		<?php elseif($active_tab == 'nes-layout-styles') : ?>
		    <h2><?php _e('Layout & Styles','nes'); ?></h2>
            <hr/>
          	<br/>
            <form method="post" action="">
                <h3><?php _e('General','nes'); ?></h3>
                <p>
                    <label style="width:auto;"><?php _e('Use Tabbed Layout on Frontend Display','nes'); ?></label>&nbsp;
                    <select name="nes_use_tabbed_layout">
                        <option value="yes" <?php if($this->nes_settings['use_tabbed_layout'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
                        <option value="no" <?php if($this->nes_settings['use_tabbed_layout'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
                    </select>
                </p>                
                <p>
                    <label style="width:auto;"><?php _e('Enable Tabbed Layout to follow user down the page','nes'); ?></label>&nbsp;
                    <select name="nes_use_tabbed_layout_follow">
                        <option value="yes" <?php if($this->nes_settings['use_tabbed_layout_follow'] == "yes"){echo 'selected';};?>><?php _e('Yes','nes'); ?></option>
                        <option value="no" <?php if($this->nes_settings['use_tabbed_layout_follow'] == "no"){echo 'selected';};?>><?php _e('No','nes'); ?></option>
                    </select>
                </p>
                <br/>
                <h3><?php _e('Styling','nes'); ?></h3>
                <h4><?php echo $this->nes_settings['event_plural']; ?></h4>
                <p class="nes-clearer">
                    <div id="nes-pending-colorpicker" class="nes-colorpicker"></div>
                    <input type="hidden" id="nes-pending-color" name="nes_pending_color" value="<?php echo $this->nes_settings['pending_color']; ?>" />
                    <label class="nes-after-input"><?php printf(__('Pending %1$s background color %2$s(default: #74ace8)%3$s','nes'),$this->nes_settings['event_single'],'<span class="description">','</span>'); ?></label>
                </p>
                <p class="nes-clearer">
                    <div id="nes-approved-colorpicker" class="nes-colorpicker"></div>
                    <input type="hidden" id="nes-approved-color" name="nes_approved_color" value="<?php echo $this->nes_settings['approved_color']; ?>" />
                    <label class="nes-after-input"><?php printf(__('Approved %1$s background color %2$s(default: #43d94d)%3$s','nes'),$this->nes_settings['event_single'],'<span class="description">','</span>'); ?></label>
                </p>
                <p class="nes-clearer">
                    <div id="nes-denied-colorpicker" class="nes-colorpicker"></div>
                    <input type="hidden" id="nes-denied-color" name="nes_denied_color" value="<?php echo $this->nes_settings['denied_color']; ?>" />
                    <label class="nes-after-input"><?php printf(__('Denied %1$s background color %2$s(default: #e87f4a)%3$s','nes'),$this->nes_settings['event_single'],'<span class="description">','</span>'); ?></label>
                </p>
                <br class="nes-clearer" />

                <h4><?php echo $this->nes_settings['location_single']; ?></h4>
                 <p class="nes-clearer">
                    <div id="nes-table-header-colorpicker" class="nes-colorpicker"></div>
                    <input type="hidden" id="nes-table-header-color" name="nes_table_header_color" value="<?php echo $this->nes_settings['table_header_color']; ?>" />
                    <label class="nes-after-input"><?php printf(__('Table Header background color %1$s(default: #dddddd)%2$s','nes'),'<span class="description">','</span>'); ?></label>
                </p>
                <p class="nes-clearer">
                    <div id="nes-unavailable-colorpicker" class="nes-colorpicker"></div>
                    <input type="hidden" id="nes-unavailable-color" name="nes_unavailable_color" value="<?php echo $this->nes_settings['unavailable_color']; ?>" />
                    <label class="nes-after-input"><?php printf(__('Unavailable timeslot background color %1$s(default: #cccccc)%2$s','nes'),'<span class="description">','</span>'); ?></label>
                </p>     
                <br class="nes-clearer" />
                
                <h4><?php _e('Custom CSS','nes'); ?></h4>
                <p>
                    <span class="description"><?php _e('This will be placed in the header of your theme','nes'); ?>.</span>  
                    <textarea name="nes_custom_css" rows="10" style="width:100%;"><?php echo $this->nes_settings['custom_css']; ?></textarea>  
                </p>
                
                <input type="submit" class="button-primary" name="nes_update_settings" value="Save Layout & Styles Settings"/> 
            </form>
            <script type="text/javascript">
                jQuery(document).ready(function($){
                    $('#nes-pending-colorpicker').colpick({
                        layout:'rgbhex',
                        color:"<?php echo $this->nes_settings['pending_color']; ?>",
                        onSubmit:function(hsb,hex,rgb,el) {
                            $(el).css('background-color', '#'+hex);
                            $(el).colpickHide();
                        },
                        onChange:function(hsb,hex,rgb,el,bySetColor) {
                            $('#nes-pending-color').val(hex);
                        }
                    }).css('background-color', "#<?php echo $this->nes_settings['pending_color']; ?>");

                    $('#nes-approved-colorpicker').colpick({
                        layout:'rgbhex',
                        color:"<?php echo $this->nes_settings['approved_color']; ?>",
                        onSubmit:function(hsb,hex,rgb,el) {
                            $(el).css('background-color', '#'+hex);
                            $(el).colpickHide();
                        },
                        onChange:function(hsb,hex,rgb,el,bySetColor) {
                            $('#nes-approved-color').val(hex);
                        }
                    }).css('background-color', "#<?php echo $this->nes_settings['approved_color']; ?>");

                    $('#nes-denied-colorpicker').colpick({
                        layout:'rgbhex',
                        color:"<?php echo $this->nes_settings['denied_color']; ?>",
                        onSubmit:function(hsb,hex,rgb,el) {
                            $(el).css('background-color', '#'+hex);
                            $(el).colpickHide();
                        },
                        onChange:function(hsb,hex,rgb,el,bySetColor) {
                            $('#nes-denied-color').val(hex);
                        }
                    }).css('background-color', "#<?php echo $this->nes_settings['denied_color']; ?>");

                    $('#nes-unavailable-colorpicker').colpick({
                        layout:'rgbhex',
                        color:"<?php echo $this->nes_settings['unavailable_color']; ?>",
                        onSubmit:function(hsb,hex,rgb,el) {
                            $(el).css('background-color', '#'+hex);
                            $(el).colpickHide();
                        },
                        onChange:function(hsb,hex,rgb,el,bySetColor) {
                            $('#nes-unavailable-color').val(hex);
                        }
                    }).css('background-color', "#<?php echo $this->nes_settings['unavailable_color']; ?>");

                    $('#nes-table-header-colorpicker').colpick({
                        layout:'rgbhex',
                        color:"<?php echo $this->nes_settings['table_header_color']; ?>",
                        onSubmit:function(hsb,hex,rgb,el) {
                            $(el).css('background-color', '#'+hex);
                            $(el).colpickHide();
                        },
                        onChange:function(hsb,hex,rgb,el,bySetColor) {
                            $('#nes-table-header-color').val(hex);
                        }
                    }).css('background-color', "#<?php echo $this->nes_settings['table_header_color']; ?>");
                });
            </script>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#nes-venue-id').chosen({
            placeholder_text_single: "<?php printf(__('Select a %1$s','nes'),$this->nes_settings['venue_single']); ?>"
        });	

		if($('select[name="nes_hide_admin_bar"]').val() == 'yes'){
			$('#nes-show-admin-bar').show();
		}
		$('select[name="nes_hide_admin_bar"]').on('change', function(){
			$('#nes-show-admin-bar').slideToggle('fast');
		});

		// show/hide end time types
		var selectedVal = "";
		var selected = $("input[type='radio'][name='nes_end_time_type']:checked");
		if(selected.length > 0){
		    selectedVal = selected.val();
		}

		if(selectedVal == 'fixed'){
			$('.event-fixed-end-times').fadeIn('fast');
		}else if(selectedVal == 'minmax'){
			$('.event-minmax-end-times').fadeIn('fast');
		}

    	// toggle fixed and min/max
    	$('.event-end-time-type label').on('click', function(){
			var selectedVal = $('input', this).val();
    		if(selectedVal == 'fixed'){
				$('.event-fixed-end-times').fadeIn('fast');
				$('.event-minmax-end-times').hide();
			}else if(selectedVal == 'minmax'){
				$('.event-minmax-end-times').fadeIn('fast');
				$('.event-fixed-end-times').hide();
			}else {
				$('.event-fixed-end-times').hide();
				$('.event-minmax-end-times').hide();
			}
		});	
    });
</script>