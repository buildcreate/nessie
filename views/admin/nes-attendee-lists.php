<div class="nes-main-wrap">
	<div id="nes-saving-wrap" style="display:none;">
		<div id="nes-saving">
			<h4><?php _e('SAVING','nes'); ?></h4>
			<i class="bts bt-spinner bt-pulse"></i>
		</div>
	</div>

	<form style="float:right;" method="post" action="">
		<button class="button"><i class="btr bt-upload"></i> Export Attendee List to .csv</button>
		<input type="hidden" name="nes_event_id" value="<?php echo $_GET['event_id']; ?>" />
		<?php wp_nonce_field('nes_export_attendee_list','nes_export_attendee_list_nonce', true, true); ?>
	</form>

	<h1 class="nes-page-title"><i class="bts bt-paste"></i> Attendee List</h1>

	<?php $post_id = htmlentities($_GET['event_id']); ?>
	<?php $event = get_post($post_id); ?>
	<div class="nes-event-info">	
		<div class="nes-what-when-where">
			<h2><a href="/wp-admin/post.php?post=<?php echo $post_id; ?>&action=edit"><?php echo get_the_title($post_id); ?></a></h2>
			<ul>
				<li><strong>When:</strong> <?php echo date('l - F jS, Y', strtotime(get_post_meta($post_id, 'nes_event_date', true))); ?> @ <?php echo date('g:ia', strtotime(get_post_meta($post_id, 'nes_start_time', true))); ?> - <?php echo date('g:ia', strtotime(get_post_meta($post_id, 'nes_end_time', true))); ?></li>
				<?php 
					// check if onsite
					$event_type = get_post_meta($post_id, 'nes_event_type', true);
					if($event_type == 'onsite'){
						// get venue
						$where = get_the_title(get_post_meta($post_id, 'nes_venue_id', true));	
						// check for locations				
						$location_ids = get_post_meta($post_id, 'nes_location_id', true);
						if($location_ids){
							$delimiter = ' - ';
							foreach($location_ids as $location_id){
								$where .= $delimiter . get_the_title($location_id);
								$delimiter = ', ';
							}
						} 
					}else{
						$where = get_post_meta($post_id, 'nes_offsite_venue_name', true);
						if($address = get_post_meta($post_id, 'nes_offsite_venue_address', true)){
							$where .= ' - <a target="_blank" href="http://maps.google.com/?q='.$address.'">'.$address.' <sup><i class="btr bt-external-link"></i></sup></a>';
						}
					}
				?>
				<?php if($where) : ?>
				<li>
					<strong>Where:</strong> <?php echo $where; ?>
				</li>
				<?php endif; ?>
			</ul>
		</div>
		<div class="nes-how-many">
			<?php $attendees = get_post_meta($post_id, 'nes_attendees', true); ?>
			<?php if($attendees) : ?>
				<h2>Tickets Sold: <?php echo count($attendees); ?></h2>
				<?php 
					// get how many are checked in
					$currently_in = 0;
					foreach($attendees as $attendee){
						if($attendee['status'] == 'in'){
							$currently_in++;
						}
					}
				?>
				<h2>Checked In: <span class="nes-currently-in"><?php echo $currently_in; ?></span></h2>				
			<?php else : ?>
				<h2>Tickets Sold: 0</h2>
				<h2>Checked In: 0</h2>
			<?php endif; ?>
		</div>
		<span class="nes-clearer"></span>
	</div>
	<table class="nes-attendee-list">
		<thead>
			<tr>
				<th>Date Purchased</th>
				<th>Ticket Number</th>
				<th>Purchaser</th>
				<th>Attendee</th>
				<th>Ticket Description</th>
				<th>Ticket Price</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php $attendees = get_post_meta($post_id, 'nes_attendees', true); ?>
			<?php if($attendees): ?>
				<?php $count = 2; ?>
				<?php foreach($attendees as $k => $attendee) : ?>
				<tr class="<?php if($count%2){echo 'even';}else{echo 'odd';} $count++; ?>">
					<td><?php echo date('F jS, Y', substr($attendee['number'], 0, -4)); ?></td>
					<td><?php echo $attendee['number']; ?></td>
					<td><?php echo $attendee['purchaser']; ?></td>
					<td><?php echo $attendee['attendee']; ?></td>
					<td><?php echo $attendee['description']; ?></td>
					<td><?php echo $attendee['price']; ?></td>
					<td class="nes-check-in-out">
						<?php if($attendee['status'] == 'in') : ?>
							<button class="button nes-check-out" value="<?php echo $k; ?>" data-action="out">Check Out</span>
						<?php else : ?>
							<button class="button nes-check-in" value="<?php echo $k; ?>" data-action="in">Check In</button>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; ?>
			<?php endif;?>
		</tbody>
	</table>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$('.nes-attendee-list').tablesorter({
				widgets: ["zebra"],
				sortList: [[0,0]]
			}); 

			// check attendee in
			$('.nes-check-in-out button').on('click', function(e){
				e.preventDefault();

				$('#nes-saving-wrap').fadeIn('fast');

				// get the button data to post
				var $button = $(this);
                var data = {
                    'action': 'nes_ajax_attendee_check_in_out',
                    'meta_key': $(this).val(),
                    'in_out': $(this).attr('data-action'),
                    'event_id': "<?php echo $_GET['event_id']; ?>"
                };

                $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){

                	// toggle the button after ajax meta save
                	$button.toggleClass('nes-check-in nes-check-out');
                	$button.text(response);
                	var currently_in = parseInt($('.nes-currently-in').text());
                	if($button.attr('data-action') == 'in'){
                		$button.attr('data-action', 'out');
						$('.nes-currently-in').text(currently_in + 1);
	                }else{
        				$button.attr('data-action', 'in');
        				$('.nes-currently-in').text(currently_in - 1);
	                }

	                // hide overlay
	                $('#nes-saving-wrap').hide();
                }); 
			});
	
		});
	</script>
</div>