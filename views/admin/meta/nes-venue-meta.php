<?php global $post; ?>
<?php wp_nonce_field('save_venue_meta', 'venue_meta_nonce'); ?>

<div id="nes-fields">
    <div class="nes-message">
        <h2>IMPORTANT: Deleting this will remove all <?php echo $this->nes_settings['location_single']; ?> and <?php echo $this->nes_settings['event_single']; ?> associations with this <?php echo $this->nes_settings['venue_single']; ?>. This CANNOT be undone!</h2>
    </div>
    <div class="nes-venue-general"> 
        <h3><?php _e('Display','nes'); ?></h3>        
        <?php 
            // get color
            $venue_color = get_post_meta($post->ID, 'nes_venue_color', true);
            if(empty($venue_color)){
                $venue_color = $this->nes_settings['pending_color'];
            }
        ?>
        <div>
            <p>Background Color:</p>
            <div id="nes-venue-colorpicker" class="nes-colorpicker"></div>
            <input type="hidden" id="nes-venue-color" name="nes_venue_color" value="<?php echo $venue_color; ?>" />
            <span class="nes-clearer"></span>
        </div>   
        <?php 
            // get text color
            $venue_text_color = get_post_meta($post->ID, 'nes_venue_text_color', true);
            if(empty($venue_text_color)){
                $venue_text_color = 'ffffff';
            }
        ?>     
        <div>
            <p>Text Color:</p>
            <div id="nes-venue-text-colorpicker" class="nes-colorpicker"></div>
            <input type="hidden" id="nes-venue-text-color" name="nes_venue_text_color" value="<?php echo $venue_text_color; ?>" />
            <span class="nes-clearer"></span>
        </div>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                $('#nes-venue-colorpicker').colpick({
                    layout:'rgbhex',
                    color:"<?php echo $venue_color; ?>",
                    onSubmit:function(hsb,hex,rgb,el) {
                        $(el).css('background-color', '#'+hex);
                        $(el).colpickHide();
                    },
                    onChange:function(hsb,hex,rgb,el,bySetColor) {
                        $('#nes-venue-color').val(hex);
                    }
                }).css('background-color', "#<?php echo $venue_color; ?>");                
                $('#nes-venue-text-colorpicker').colpick({
                    layout:'rgbhex',
                    color:"<?php echo $venue_text_color; ?>",
                    onSubmit:function(hsb,hex,rgb,el) {
                        $(el).css('background-color', '#'+hex);
                        $(el).colpickHide();
                    },
                    onChange:function(hsb,hex,rgb,el,bySetColor) {
                        $('#nes-venue-text-color').val(hex);
                    }
                }).css('background-color', "#<?php echo $venue_text_color; ?>");
            });
        </script>
        <h3><?php _e('Address','nes'); ?></h3>
        <p>
            <label for="nes-address"><?php _e('Street Address','nes'); ?></label><br/>
            <input type="text" name="nes_address" id="nes-address" value="<?php echo get_post_meta($post->ID, 'nes_address', true); ?>" />
        </p>            
        <p>
            <label for="nes-city"><?php _e('City','nes'); ?></label><br/>
            <input type="text" name="nes_city" id="nes-city" value="<?php echo get_post_meta($post->ID, 'nes_city', true); ?>" />
        </p>            
        <p>
            <label for="nes-state"><?php _e('State or Province (abbreviation)','nes'); ?></label><br/>
            <input type="text" name="nes_state" id="nes-state" value="<?php echo get_post_meta($post->ID, 'nes_state', true); ?>" />
        </p>            
        <p>
            <label for="nes-zipcode"><?php _e('Zipcode','nes'); ?></label><br/>
            <input type="text" name="nes_zipcode" id="nes-zipcode" value="<?php echo get_post_meta($post->ID, 'nes_zipcode', true); ?>" />
        </p>            
        <p>
            <label for="nes-country"><?php _e('Country','nes'); ?></label><br/>
            <input type="text" name="nes_country" id="nes-country" value="<?php echo get_post_meta($post->ID, 'nes_country', true); ?>" />
        </p>    
        <p>
            <?php $nes_google_map = get_post_meta($post->ID, 'nes_google_map', true); ?>
            Show Google Map for this <?php echo $this->nes_settings['venue_single']; ?><br/>
            <label><input type="radio" name="nes_google_map" value="yes" <?php if((empty($nes_google_map)) || $nes_google_map == 'yes'){echo 'checked="checked"';} ?>> Yes</label>
            <label>&nbsp;&nbsp;<input type="radio" name="nes_google_map" value="no" <?php if($nes_google_map == 'no'){echo 'checked="checked"';} ?>/> No</label>
        </p> 
        <h3><?php _e('Contact Information','nes'); ?></h3>
        <p>
            <label for="nes-contact-email"><?php _e('Contact Email','nes'); ?></label><br/>
            <input type="text" name="nes_contact_email" id="nes-contact-email" value="<?php echo get_post_meta($post->ID, 'nes_contact_email', true); ?>" />
        </p> 
        <p>
            <label for="nes-phone"><?php _e('Phone','nes'); ?></label><br/>
            <input type="text" name="nes_phone" id="nes-phone" value="<?php echo get_post_meta($post->ID, 'nes_phone', true); ?>" />
        </p>   
        <p>
            <label for="nes-website"><?php _e('Website','nes'); ?></label><br/>
            <input type="text" name="nes_website" id="nes-website" value="<?php echo get_post_meta($post->ID, 'nes_website', true); ?>" />
        </p>         

    </div>

    <div class="nes-availability">  
        <h3><?php printf(__('%1$s availability','nes'),$this->nes_settings['event_single']); ?>. <em>(<?php printf(__('Leave times blank to make this %1$s unavailable on those days','nes'),$this->nes_settings['venue_single']); ?>)</em></h3>
        <?php $days = array(__('Monday','nes'), __('Tuesday','nes'), __('Wednesday','nes'), __('Thursday','nes'), __('Friday','nes'), __('Saturday','nes'), __('Sunday','nes')); ?>
        <?php foreach($days as $day) : ?>
        <div>
            <h4><?php echo $day; ?></h4>
            <label><?php _e('Start','nes'); ?></label><br/>
            <?php echo $this->nes_get_time_select('nes_venue_'.strtolower($day).'_start', get_post_meta($post->ID, 'nes_venue_'. strtolower($day). '_start', true), null, false, false, false, 0.25, false, true); ?><br/>
            <label><?php _e('End','nes'); ?></label><br/>
            <?php echo $this->nes_get_time_select('nes_venue_'.strtolower($day).'_end', get_post_meta($post->ID, 'nes_venue_'. strtolower($day). '_end', true), null, false, false, false, 0.25, false, true); ?><br/>
        </div>  
        <?php endforeach; ?> 
        <span class="nes-clearer"></span> 
        <br/>
    </div>

    <div class="nes-blackouts">
        <h3>Blackout Dates/Times</h3>
        <?php $times = $this->nes_get_times(0,23.75,0.25); ?>
        <?php $locations = $this->nes_get_venue_locations($post->ID); ?>
        <table class="nes-blackout-list">
            <thead>
                <tr>
                    <th><?php echo $this->nes_settings['location_plural']; ?></th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $blackout_locations_arr = get_post_meta($post->ID, 'nes_blackout_locations', true);
                    $blackout_locations = array();
                    if($blackout_locations_arr){
                        foreach($blackout_locations_arr as $blackout_location_arr){
                         
                            $blackout_locations[] = $blackout_location_arr;
                        }
                    }
                    $blackout_start_date = get_post_meta($post->ID, 'nes_blackout_start_date', true);
                    $blackout_end_date = get_post_meta($post->ID, 'nes_blackout_end_date', true);
                    $blackout_start_time = get_post_meta($post->ID, 'nes_blackout_start_time', true);
                    $blackout_end_time = get_post_meta($post->ID, 'nes_blackout_end_time', true);
                ?>
                <?php if($blackout_start_date) : ?>
                    <?php $first = true; ?>
                    <?php foreach($blackout_start_date as $key => $value) : ?>
                        <tr class="nes-repeater-item">
                            <td class="nes-blackout-locations">
                                <select name="nes_blackout_locations[<?php echo rand(); ?>][]" multiple>
                                    <option value=""></option>
                                    <?php if($locations) : ?>
                                        <?php foreach($locations as $location) : ?>
                                            <option value="<?php echo $location->ID; ?>" <?php if(is_array($blackout_locations[$key]) && in_array($location->ID, $blackout_locations[$key])){echo 'selected="selected"';} ?>><?php echo $location->post_title; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </td>
                            <td>
                                <input type="date" name="nes_blackout_start_date[]" value="<?php echo $blackout_start_date[$key]; ?>" />
                            </td>                    
                            <td>
                                <input type="date" name="nes_blackout_end_date[]" value="<?php echo $blackout_end_date[$key]; ?>" />
                            </td>
                            <td>
                                <select name="nes_blackout_start_time[]">
                                    <option value=""></option>
                                    <?php foreach($times as $time) : ?>
                                        <option value="<?php echo $time; ?>" <?php if($time == $blackout_start_time[$key]){echo 'selected';} ?>>
                                            <?php echo date('g:i a', strtotime($time)); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>                    
                            <td>
                                <select name="nes_blackout_end_time[]">
                                    <option value=""></option>
                                    <?php foreach($times as $time) : ?>
                                        <option value="<?php echo $time; ?>" <?php if($time == $blackout_end_time[$key]){echo 'selected';} ?>>
                                            <?php echo date('g:i a', strtotime($time)); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <i class="btr bt-plus-circle"></i>
                                <?php if(!$first) : ?>
                                    <i class="btr bt-minus-circle"></i>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <?php $first = false; ?>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr class="nes-repeater-item">
                        <td class="nes-blackout-locations">
                            <select name="nes_blackout_locations[<?php echo rand(); ?>][]" multiple>
                                <option value=""></option>
                                <?php if($locations) : ?>
                                    <?php foreach($locations as $location) : ?>
                                        <option value="<?php echo $location->ID; ?>"><?php echo $location->post_title; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </td>
                        <td>
                            <input type="date" name="nes_blackout_start_date[]" />
                        </td>                    
                        <td>
                            <input type="date" name="nes_blackout_end_date[]" />
                        </td>
                        <td>
                            <select name="nes_blackout_start_time[]">
                                <option value=""></option>
                                <?php foreach($times as $time) : ?>
                                    <option value="<?php echo $time; ?>" <?php if($time == '00:00'){echo 'selected';} ?>>
                                        <?php echo date('g:i a', strtotime($time)); ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </td>                    
                        <td>
                            <select name="nes_blackout_end_time[]">
                                <option value=""></option>
                                <?php foreach($times as $time) : ?>
                                    <option value="<?php echo $time; ?>" <?php if($time == '23:45'){echo 'selected';} ?>>
                                        <?php echo date('g:i a', strtotime($time)); ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                        <td>
                            <i class="btr bt-plus-circle"></i>
                            <i class="btr bt-minus-circle"></i>
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <script type="text/javascript">
            jQuery(document).ready(function($){
                // define what a new row is
                var new_row = '<tr class="nes-repeater-item">' +
                    '<td class="nes-blackout-locations">' +
                        '<select name="" multiple>' +
                            '<option value=""></option>' +
                            <?php if($locations) : ?>
                                <?php foreach($locations as $location) : ?>
                                    '<option value="<?php echo $location->ID; ?>"><?php echo $location->post_title; ?></option>' +
                                <?php endforeach; ?>
                            <?php endif; ?>
                        '</select>' +
                    '</td>' +
                    '<td>' +
                        '<input type="date" name="nes_blackout_start_date[]" />' +
                    '</td>' +                 
                    '<td>' +
                        '<input type="date" name="nes_blackout_end_date[]" />' +
                    '</td>' +
                    '<td>' +
                        '<select name="nes_blackout_start_time[]">' +
                            '<option value=""></option>' +
                            <?php foreach($times as $time) : ?>
                                '<option value="<?php echo $time; ?>"<?php if($time == "00:00"){echo " selected";} ?>><?php echo date('g:i a', strtotime($time)); ?></option>' +
                            <?php endforeach; ?>
                        '</select>' +
                    '</td>' +                  
                    '<td>' +
                        '<select name="nes_blackout_end_time[]">' +
                            '<option value=""></option>' +
                            <?php foreach($times as $time) : ?>
                                '<option value="<?php echo $time; ?>"<?php if($time == "23:45"){echo " selected";} ?>><?php echo date('g:i a', strtotime($time)); ?></option>' +
                            <?php endforeach; ?>
                        '</select>' +
                    '</td>' +
                    '<td>' +
                        '<i class="btr bt-plus-circle"></i>' +
                        '<i class="btr bt-minus-circle"></i>' +
                    '</td>' +
                '</tr>';

                // add rows
                $(document).on('click', '.nes-blackouts .bt-plus-circle', function(){
                    $(this).parents('.nes-repeater-item').after(new_row);
                    
                    // add random "name"
                    var rand = 1 + Math.floor(Math.random() * 999999999);
                    $(this).parents('.nes-repeater-item').next('tr').find('.nes-blackout-locations select').attr('name', 'nes_blackout_locations['+rand+'][]');
                    // setup chosen
                    $(this).parents('.nes-repeater-item').next('tr').find('.nes-blackout-locations select').chosen();
                });

                // remove rows
                $(document).on('click', '.nes-blackouts .bt-minus-circle', function(){
                    $(this).parents('.nes-repeater-item').fadeOut('fast').remove();
                }); 

                // setup chosen
                $('.nes-blackout-locations select').chosen();
            });
        </script>
    </div>
</div>