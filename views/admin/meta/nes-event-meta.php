<?php global $post; ?>
<?php $post_id = $post->ID; ?>
<?php wp_nonce_field('save_event_meta', 'event_meta_nonce'); ?>

<div id="nes-fields">
    <?php $series = get_post_meta($post_id,'nes_recurring_series',true); ?>
    <?php if($series) : ?>
        <div class="nes-recurring-series">
            <h3>This is the <?php echo $this->nes_ordinal((array_search($post_id, array_keys($series)) + 1)); ?> of <?php echo count($series); ?> <?php echo $this->nes_settings['event_plural']; ?> in this recurring series.
                <a href="<?php echo admin_url().'edit.php?post_type=nes_event&nes_event_ids[]='.implode('&nes_event_ids[]=', array_keys($series)); ?>">(View all)</a>
            </h3>
            <hr/>
            <p>
                <label><input type="radio" name="nes_edit_type" value="all" checked="checked"/> Edit All <?php echo $this->nes_settings['event_plural']; ?> in this series&nbsp;&nbsp;</label>
                <label><input type="radio" name="nes_edit_type" value="upcoming"/> Edit Upcoming <?php echo $this->nes_settings['event_plural']; ?>&nbsp;&nbsp;</label>
                <label><input type="radio" name="nes_edit_type" value="one"/> Edit this <?php echo $this->nes_settings['event_single']; ?> only</label>
            </p>
            <div class="nes-all-dialog nes-dialog" title="IMPORTANT: ALL EVENTS" style="display:none;">
                <h4>This option will update all <?php echo $this->nes_settings['event_plural']; ?> in this series.</h4>
                <ul>
                    <li>Please ensure the fields in the "Event Date/Time" section are correct</li>
                    <li>Please use the "Check Availability" button to ensure availability (if on-site)</li>
                    <li>Events will not be created on dates that are unavailable</li>
                </ul>
            </div>              
            <div class="nes-upcoming-dialog nes-dialog" title="IMPORTANT: UPCOMING EVENTS" style="display:none;">
                <h4>This option will create a new upcoming series. All previous <?php echo $this->nes_settings['event_plural']; ?> will be broken into their own series. This cannot be undone.</h4>
                <ul>
                    <li>Please ensure the fields in the "Event Date/Time" section are correct</li>
                    <li>Please use the "Check Availability" button to ensure availability (if on-site)</li>
                    <li>Events will not be created on dates that are unavailable</li>
                </ul>
            </div>            
            <div class="nes-one-dialog nes-dialog" title="IMPORTANT: ONE TIME EVENT" style="display:none;">
                <h4>This option will break this <?php echo $this->nes_settings['event_single']; ?> from this series and cannot be undone.</h4>
                <ul>
                    <li>Please ensure the fields in the "Event Date/Time" section are correct</li>
                    <li>Please use the "Check Availability" button to ensure availability (if on-site)</li>
                    <li>Events will not be created on dates that are unavailable</li>
                </ul>
            </div>
        </div>
    <?php endif; ?>

    <h3 class="nes-section-title"><?php echo $this->nes_settings['venue_single']; ?>/<?php echo $this->nes_settings['location_single']; ?> Information<i class="btr bt-angle-down"></i></h3>
    <div class="nes-event-details-section nes-meta-section">
        <div class="nes-event-type">
            <p>
                <label class="nes-styled">This <?php echo $this->nes_settings['event_single']; ?> is</label>
                <?php $nes_event_type = get_post_meta($post_id, 'nes_event_type', true); ?>
                <label><input type="radio" name="nes_event_type" value="offsite" <?php if($nes_event_type == 'offsite' || empty($nes_event_type)){echo 'checked="checked"';}?>/> Off-site</label>&nbsp;&nbsp;
                <label><input type="radio" name="nes_event_type" value="onsite" <?php if($nes_event_type == 'onsite'){echo 'checked="checked"';}?>/> On-site</label>
            </p>
        </div>           

        <div class="nes-venue">
            <?php $venue_id = false; ?>
            <?php $venues = $this->nes_get_venues();  ?>
            <?php if($venues) : ?>
            <p class="nes-onsite-venue" style="display:none;">
                <label class="nes-styled"><?php echo $this->nes_settings['venue_single']; ?>: </label>
                <select id="nes-venue-id" name="nes_venue_id">
                <?php foreach($venues as $venue) : ?> 
                    <option value="<?php echo $venue->ID; ?>" 
                        <?php
                            if($venue->ID == get_post_meta($post_id, 'nes_venue_id', true)){
                                echo 'selected';
                                $venue_id = $venue->ID;
                            }else if(get_option('nes_default_venue') == $venue->ID){
                                echo 'selected';
                                $venue_id = $venue->ID;
                            }
                        ?>
                    ><?php echo get_the_title($venue->ID); ?></option>
                <?php endforeach; ?>
                </select>
            </p>
            <?php endif; ?>

            <div class="nes-offsite-venue">
                <p>
                    <label class="nes-styled"><?php echo $this->nes_settings['venue_single']; ?> Name</label>
                    <input type="text" name="nes_offsite_venue_name" value="<?php echo get_post_meta($post_id,'nes_offsite_venue_name',true); ?>" />
                </p>
                <p>
                    <label class="nes-styled"><?php echo $this->nes_settings['venue_single']; ?> Address</label>
                    <input type="text" name="nes_offsite_venue_address" value="<?php echo get_post_meta($post_id,'nes_offsite_venue_address',true); ?>" />
                </p>
            </div>
        </div>  

        <div id="nes-locations" style="display:none;">
            <?php // locations will only be relevant if a venue ID has been set ?>
            <?php if($venue_id) : ?>
                <?php $locations = $this->nes_get_venue_locations($venue_id); ?>
                <?php if($locations) : ?>
                <p>
                    <label class="nes-styled"><?php echo $this->nes_settings['location_plural']; ?>: </label>
                    <select id="nes-location-id" name="nes_location_id[]" multiple>
                    <option></option>
                    <?php foreach($locations as $location) : ?>
                        <option value="<?php echo $location->ID; ?>" 
                            <?php 
                                $location_ids = get_post_meta($post_id, 'nes_location_id', true);
                                if(!empty($location_ids)){
                                    if(is_array($location_ids)){
                                        if(in_array($location->ID, $location_ids)){
                                            echo "selected";
                                        }
                                    }
                                    else if($location->ID == $location_ids){
                                        echo "selected";
                                    }
                                } 
                            ?>
                        ><?php echo $location->post_title; ?></option>
                    <?php endforeach; ?>
                    </select>
                </p>
                <?php endif; ?>
            <?php endif; ?>
        </div>

        <div class="nes-private-public">
            <p>
                <label class="nes-styled"><?php echo $this->nes_settings['event_single']; ?> visibility</label>
                <?php $nes_private_public = get_post_meta($post_id, 'nes_private_public', true); ?>
                <label><input type="radio" name="nes_private_public" value="public" <?php if($nes_private_public == 'public' || empty($nes_private_public)){echo 'checked="checked"';}?>/> Public (show on calendar)</label>&nbsp;&nbsp;
                <label><input type="radio" name="nes_private_public" value="private" <?php if($nes_private_public == 'private'){echo 'checked="checked"';}?>/> Private</label>
            </p>
        </div>

        <div class="nes-event-status">
            <p>
                <label class="nes-styled"><?php echo $this->nes_settings['event_single']; ?> Status</label>
                <select id="nes-event-status" name="nes_event_status">
                    <?php $stauses = array('Pending', 'Approved', 'Denied', 'Blocked'); ?>
                    <?php foreach($stauses as $status) : ?>
                        <option value="<?php echo strtolower($status); ?>" 
                            <?php if(get_post_meta($post_id, 'nes_event_status', true) == strtolower($status)){echo 'selected';} ?>
                        ><?php echo $status; ?></option>
                    <?php endforeach; ?>
                </select>
                <span class="description">"Approved" is necessary for <?php echo $this->nes_settings['event_plural']; ?> to appear on the calendar.</span>
            </p>
            <div>
                <label>Comments sent to the submittor when the <?php echo $this->nes_settings['event_single']; ?> status changes</label><br/>
                <textarea name="nes_event_comments" rows="6" cols="80"><?php echo get_post_meta($post_id, 'nes_event_comments', true); ?></textarea>
            </div>
        </div>
    </div>

    <h3 class="nes-section-title"><?php echo $this->nes_settings['event_single']; ?> Date/Time <i class="btr bt-angle-down"></i></h3>  
    <div class="nes-event-date-time-section nes-meta-section">
        <p>
            <label class="nes-styled"><?php _e('Event Date','nes'); ?></label> <input id="nes-event-datepicker" type="text" value="<?php if($event_date = get_post_meta($post_id, 'nes_event_date', true)){echo date('m/d/Y', strtotime($event_date));} ?>" />
            <input id="nes-event-date" name="nes_event_date" type="hidden" value="<?php if(get_post_meta($post_id, 'nes_event_date', true)){echo get_post_meta($post_id, 'nes_event_date', true);} ?>" />
        </p>
        <p class="nes-setup-time" style="display:none;">
            <label class="nes-styled"><?php printf(__('Setup Start Time (before %1$s)','nes'),$this->nes_settings['event_single']); ?></label>
            <?php 
                // dont set this because if blank it sets to start time later
                if(!($value = get_post_meta($post_id, 'nes_start_setup_time', true))){
                    $value = '12:00';
                } 
                echo $this->nes_get_time_select('nes_start_setup_time', $value);
            ?>
        </p> 
        <p>
            <label class="nes-styled"><?php _e('Start Time','nes'); ?></label>
            <?php 
                if(!($value = get_post_meta($post_id, 'nes_start_time', true))){
                    $value = '12:00';
                } 
                echo $this->nes_get_time_select('nes_start_time', $value);
            ?>
        </p>
        <p>
            <label class="nes-styled"><?php _e('End Time','nes'); ?></label>
            <?php 
                if(!($value = get_post_meta($post_id, 'nes_end_time', true))){
                    $value = '12:00';
                } 
                echo $this->nes_get_time_select('nes_end_time', $value);
            ?>
        </p>  
        <p class="nes-cleanup-time" style="display:none;">
            <label class="nes-styled"><?php printf(__('Cleanup End Time (after %1$s)','nes'),$this->nes_settings['event_single']); ?></label>
            <?php 
                // dont auto set this since if blank it sets to the end time later
                if(!($value = get_post_meta($post_id, 'nes_end_cleanup_time', true))){
                    $value = '12:00';
                } 
                echo $this->nes_get_time_select('nes_end_cleanup_time', $value);
            ?>
        </p>

        <p class="nes-frequency-type">
            <label class="nes-styled"><?php echo $this->nes_settings['event_single']; ?> frequency</label>
            <?php $nes_frequency_type = get_post_meta($post_id,'nes_frequency_type',true); ?>
            <?php if(!$series) : // only show if not in a series ?>
                <label><input type="radio" name="nes_frequency_type" value="one" <?php if(empty($nes_frequency_type) || $nes_frequency_type == 'one'){echo 'checked="checked"';}?>/> One Time&nbsp;&nbsp;</label> 
            <?php endif; ?>
            <label><input type="radio" name="nes_frequency_type" value="daily" <?php if($nes_frequency_type  == 'daily'){echo 'checked="checked"';}?>/> Daily&nbsp;&nbsp;</label> 
            <label><input type="radio" name="nes_frequency_type" value="weekly" <?php if($nes_frequency_type  == 'weekly'){echo 'checked="checked"';}?>/> Weekly&nbsp;&nbsp;</label> 
            <label><input type="radio" name="nes_frequency_type" value="monthly" <?php if($nes_frequency_type  == 'monthly'){echo 'checked="checked"';}?>/> Monthly&nbsp;&nbsp;</label>
            <label><input type="radio" name="nes_frequency_type" value="custom" <?php if($nes_frequency_type  == 'custom'){echo 'checked="checked"';}?>/> Custom</label>
            <span class="nes-clearer"></span>
        </p>

        <div class="nes-recurrence" style="display:none;">    
            <div class="nes-custom-datepicker-wrap" style="display:none;">
                <div class="nes-custom-dates">
                    <label>Series Dates</label> 
                    <div class="nes-custom-date-display"></div>
                    <?php 
                        // get custom dates if necessary
                        $custom_date_string = '';
                        if($custom_dates = get_post_meta($post_id, 'nes_recurring_series', true)){
                            if(is_array($custom_dates)){
                                foreach($custom_dates as $custom_date){
                                   $formatted_dates[] = date('m/d/Y', strtotime($custom_date)); 
                                }
                                $custom_date_string = "'" . implode("', '", $formatted_dates) . "'";
                            }else{
                                $custom_date_string = "'" . $custom_dates . "'";
                            }
                            
                        }
                    ?>
                    <input id="nes-custom-date" name="nes_custom_date" type="hidden" value="<?php if($custom_dates = get_post_meta($post_id, 'nes_recurring_series', true)){echo implode(', ', get_post_meta($post_id, 'nes_recurring_series', true));} ?>" />
                </div>
                <div id="nes-custom-datepicker"></div>
                <span class="nes-clearer"></span>
            </div>
            <select class="nes-frequency-monthly" name="nes_frequency_monthly" style="display:none;">
                <?php $days = array('First','Second','Third','Fourth','Last'); ?>
                <option value="">&mdash;</option>
                <?php foreach($days as $day) : ?>
                    <option value="<?php echo strtolower($day); ?>" <?php if(get_post_meta($post_id,'nes_frequency_monthly',true) == strtolower($day)){echo 'selected="selected"';}?>><?php echo $day; ?></option>
                <?php endforeach; ?>
            </select>
            <span class="nes-frequency-weekly" style="display:none;">
                <?php $days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>
                <?php foreach($days as $day) : ?>
                    <label><input type="checkbox" name="nes_frequency_weekly[]" value="<?php echo strtolower($day); ?>" <?php $nes_frequency_weekly = get_post_meta($post_id,'nes_frequency_weekly',true); if(is_array($nes_frequency_weekly) && in_array(strtolower($day), $nes_frequency_weekly)){echo 'checked="checked"';}?>/> <?php echo $day; ?>&nbsp;&nbsp;</label> 
                <?php endforeach; ?>
            </span>  
            <?php if($series_start_date = get_post_meta($post_id, 'nes_series_start_date', true)) : ?> 
            <p class="nes-series-start-date nes-styled" style="display:none;">
                <label class="nes-styled"><?php _e('Series Start Date','nes'); ?></label>
                <input type="text" name="nes_series_start_date_display" value="<?php echo date('m/d/Y', strtotime($series_start_date)); ?>" readonly />
                <input type="hidden" name="nes_series_start_date" value="<?php echo date('Y-m-d', strtotime($series_start_date)); ?>"  />
            </p>     
            <?php endif; ?>      
            <p class="nes-frequency-end-date" style="display:none;">
                <label class="nes-styled">Series End Date</label> <input id="nes-frequency-end-datepicker" type="text" value="<?php if($nes_frequency_end_date = get_post_meta($post_id, 'nes_frequency_end_date', true)){echo date('m/d/Y', strtotime($nes_frequency_end_date));} ?>" />
                <input id="nes-frequency-end-date" name="nes_frequency_end_date" type="hidden" value="<?php if($nes_frequency_end_date = get_post_meta($post_id, 'nes_frequency_end_date', true)){echo $nes_frequency_end_date;} ?>" />
            </p>
        </div>

        <div class="nes-check-availability" style="display:none;">
            <div class="nes-availability-wrap"></div>
            <button class="button nes-check-availability-button">Check Availability <i class="bts bt-spinner bt-pulse" style="display:none;"></i></button>
        </div>
    </div>
        
    <h3 class="nes-section-title"><?php echo $this->nes_settings['event_single']; ?> Needs <i class="btr bt-angle-up"></i></h3>
    <div class="nes-needs nes-meta-section" style="display:none;">
    	<p class="nes-setup-needs">
    		<label><?php _e('Set Up Needs','nes'); ?></label></br/>
    		<textarea name="nes_event_setup" rows="6" cols="80"><?php echo get_post_meta($post_id, 'nes_event_setup', true); ?></textarea>
    	</p>		
    	<p class="nes-av-needs">
    	  <label><?php _e('A/V Tech Needs','nes'); ?></label></br/>
          <textarea name="nes_event_av" rows="6" cols="80"><?php echo get_post_meta($post_id, 'nes_event_av', true); ?></textarea>
        </p>
    </div>

    <h3 class="nes-section-title"><?php _e('Contact Information','nes'); ?> <i class="btr bt-angle-up"></i></h3>
    <div class="nes-contact-information-section nes-meta-section" style="display:none;">
        <p>
            <label class="nes-styled"><?php _e('Name','nes'); ?> </label>
            <input type="text" name="nes_event_name" value="<?php echo get_post_meta($post_id, 'nes_event_name', true); ?>" />
        </p> 
        <p>
            <label class="nes-styled"><?php _e('Phone','nes'); ?> </label>
            <input type="tel" name="nes_event_phone" value="<?php echo get_post_meta($post_id, 'nes_event_phone', true); ?>" />
        </p>  
        <p>
            <label class="nes-styled"><?php _e('Email','nes'); ?> </label>
            <input type="email" name="nes_event_email" value="<?php echo get_post_meta($post_id, 'nes_event_email', true); ?>" />
        </p>        
        <p>
            <label class="nes-styled"><?php _e('Website','nes'); ?> </label>
            <input type="url" name="nes_event_website" value="<?php echo get_post_meta($post_id, 'nes_event_website', true); ?>" />
        </p>
    </div>
    
    
    <h3 class="nes-section-title">Ticket Information <i class="btr bt-angle-up"></i></h3>
    <div class="nes-ticketed-section nes-meta-section" style="display:none;">    
        <p>
            <label class="nes-styled">Is this <?php echo $this->nes_settings['event_single']; ?> Ticketed?</label>
            <?php $nes_ticketed_event = get_post_meta($post_id, 'nes_ticketed_event', true); ?>
            <label><input type="radio" name="nes_ticketed_event" value="no" <?php if($nes_ticketed_event == 'no' || empty($nes_ticketed_event)){echo 'checked="checked"';}?>/> No</label>&nbsp;&nbsp;
            <label><input type="radio" name="nes_ticketed_event" value="yes" <?php if($nes_ticketed_event == 'yes'){echo 'checked="checked"';}?>/> Yes</label>
        </p>

        <div class="nes-ticket-info" style="display:none;">        
            <p>
                <label class="nes-styled">Require login for ticket purchase?</label>
                <?php $nes_ticket_login = get_post_meta($post_id, 'nes_ticket_login', true); ?>
                <label><input type="radio" name="nes_ticket_login" value="no" <?php if($nes_ticket_login == 'no' || empty($nes_ticket_login)){echo 'checked="checked"';}?>/> No</label>&nbsp;&nbsp;
                <label><input type="radio" name="nes_ticket_login" value="yes" <?php if($nes_ticket_login == 'yes'){echo 'checked="checked"';}?>/> Yes</label>
            </p>        
            <p>
                <label class="nes-styled">Limit to one ticket per purchase?</label>
                <?php $nes_ticket_per_purchase = get_post_meta($post_id, 'nes_ticket_per_purchase', true); ?>
                <label><input type="radio" name="nes_ticket_per_purchase" value="no" <?php if($nes_ticket_per_purchase == 'no' || empty($nes_ticket_per_purchase)){echo 'checked="checked"';}?>/> No</label>&nbsp;&nbsp;
                <label><input type="radio" name="nes_ticket_per_purchase" value="yes" <?php if($nes_ticket_per_purchase == 'yes'){echo 'checked="checked"';}?>/> Yes</label>
            </p>
        
            <?php if($attendees = get_post_meta($post_id,'nes_attendees',true)) : ?>
                <p class="nes-attendee-link"><?php echo count($attendees); ?> Tickets have been purchased for this event. <a href="/wp-admin/admin.php?page=nes-attendee-lists&event_id=<?php echo $post_id; ?>">View Attendee List</a>
            <?php endif; ?>
            <p>
                <?php if(class_exists(RGFormsModel)) : ?>
                    <?php $forms = RGFormsModel::get_forms(null, 'title'); ?>
                    <?php if($forms) : ?>
                        <?php $nes_gravity_form = get_post_meta($post_id,'nes_gravity_form',true); ?>
                        <label for="nes-gravity-form" class="nes-styled">Ticket Form</label> 
                        <select id="nes-gravity-form" name="nes_gravity_form" class="nes-gravity-forms">
                            <?php $default_form_id = $this->nes_settings['default_ticket_form_id']; ?>
                            <?php foreach($forms as $form) : ?>
                                <option value="<?php echo $form->id; ?>" <?php if((empty($nes_gravity_form) && $default_form_id == $form->id) || ($nes_gravity_form == $form->id)){echo 'selected="selected"';}?>><?php echo $form->title; ?></option>
                            <?php endforeach; ?>
                        </select>
                    <?php else : ?>
                        <?php printf(__('No Gravity forms have been created yet. %1$sPlease create one here first%2$s','nes'),'<a href="/wp-admin/admin.php?page=gf_new_form">','</a>'); ?>
                    <?php endif; ?>
                <?php else : ?>
                    <?php _e('Gravity forms is not installed.', 'nes'); ?>
                <?php endif; ?>
            </p>
            <p>
                <label class="nes-styled">Ticket Limit</label>
                <input type="number" name="nes_tickets_available" min="0" value="<?php if($available = get_post_meta($post_id,'nes_tickets_available',true)){echo $available;}else{echo '0';} ?>" />
            </p>
            <div class="nes-gf-form-fields"></div>

            <table class="nes-tickets">
                <thead>
                    <tr>
                        <th class="nes-ticket-type">Ticket Type</th>
                        <th class="nes-ticket-description">Ticket Description</th>
                        <th class="nes-ticket-price">Ticket/Minimum Price</th>
                        <th class="nes-ticket-suggested">Suggested Price</th>
                        <th class="nes-add-remove">&nbsp;</th>
                    </tr>  
                </thead>
                <tbody>
                    <?php // get all repeater columns ?>
                    <?php $nes_ticket_type = get_post_meta($post_id,'nes_ticket_type',true); ?>
                    <?php $nes_ticket_description = get_post_meta($post_id,'nes_ticket_description',true); ?>
                    <?php $nes_ticket_price = get_post_meta($post_id,'nes_ticket_price',true); ?>
                    <?php $nes_ticket_suggested = get_post_meta($post_id,'nes_ticket_suggested',true); ?>
                    <?php if($nes_ticket_type) : ?>
                        <?php for($i = 0; $i < count($nes_ticket_type); $i++) : ?>
                            <tr>
                                <td class="nes-ticket-type">
                                    <select name="nes_ticket_type[]">
                                        <option value="standard">Standard</option>
                                        <option value="user-defined" <?php if($nes_ticket_type[$i] == 'user-defined'){echo 'selected';} ?>>User Defined</option>
                                    </select>
                                </td>        
                                <td class="nes-ticket-description">
                                    <input type="text" name="nes_ticket_description[]" value="<?php echo $nes_ticket_description[$i]; ?>" />
                                </td>                                                      
                                <td class="nes-ticket-price">
                                    <input type="number" name="nes_ticket_price[]" min="0" step="any" value="<?php echo $nes_ticket_price[$i]; ?>" />
                                </td>                                
                                <td class="nes-ticket-suggested">
                                    <input type="number" name="nes_ticket_suggested[]" min="0" step="any" value="<?php echo $nes_ticket_suggested[$i]; ?>" <?php if($nes_ticket_type[$i] == 'standard'){echo 'disabled="disabled"';} ?> />
                                </td>
                                <td class="nes-add-remove-ticket">
                                    <i class="bts bt-plus-circle"></i><?php if($i > 0) : ?><i class="bts bt-minus-circle"></i><?php endif; ?>
                                </td>
                            </tr>     
                        <?php endfor; ?>               
                    <?php else : ?>
                        <tr>
                            <td class="nes-ticket-user-defined">
                                <select name="nes_ticket_type[]">
                                    <option value="standard">Standard</option>
                                    <option value="user-defined">User Defined</option>
                                </select>
                            </td>        
                            <td class="nes-ticket-description">
                                <input type="text" name="nes_ticket_description[]" value="" />
                            </td>                                                      
                            <td class="nes-ticket-price">
                                <input type="number" name="nes_ticket_price[]" min="0" step="any" value="" />
                            </td>                                
                            <td class="nes-ticket-price">
                                <input type="number" name="nes_ticket_suggested[]" min="0" step="any" value="" />
                            </td>
                            <td class="nes-add-remove-ticket"><i class="bts bt-plus-circle"></i></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div> 
</div>

<div class="nes-missing-fields nes-dialog" title="VALIDATION FAILED" style="display:none;">
    <h4>Please review the highlighted fields below:</h4>
    <ul class="nes-error-list"><li></li></ul>
</div> 

<script type="text/javascript">
    jQuery(document).ready(function($){
        // run chosen on load 
        $('#nes-location-id').chosen({
            placeholder_text_multiple: "<?php printf(__('Select some %1$s','nes'),$this->nes_settings['location_plural']); ?>",
            width: "50%"
        });

        // setup datepickers
        $('#nes-event-datepicker').datepicker({
            dateFormat: "mm/dd/yy",
            altField: "#nes-event-date",
            altFormat: "yy-mm-dd",
            showOtherMonths: true,
            minDate: 0,
            showOn: "both", 
            buttonText: "<i class='btr bt-calendar'></i>",
        });                  
        $('#nes-frequency-end-datepicker').datepicker({
            dateFormat: "mm/dd/yy",
            altField: "#nes-frequency-end-date",
            altFormat: "yy-mm-dd",
            showOtherMonths: true,
            minDate: 0,
            showOn: "both", 
            buttonText: "<i class='btr bt-calendar'></i>",
        });


        // multidatepicker
        $('#nes-custom-datepicker').multiDatesPicker({
            dateFormat: "mm/dd/yy",
            altField: "#nes-custom-date",
            altFormat: "yy-mm-dd",
            showOtherMonths: true,
            minDate: 0,
            <?php if($custom_date_string) : ?>
            addDates: [<?php echo $custom_date_string; ?>],
            defaultDate: "<?php echo date('m/d/Y', strtotime($event_date)); ?>",
            <?php endif; ?>
            onSelect: function(selectedDate){
                $('#nes-custom-date').trigger('change');
                $('.nes-custom-date-display').html($('#nes-custom-date').val());
            }
        });
        $('.nes-custom-date-display').html($('#nes-custom-date').val());
       
        // ajax call to populate locations for selected venue
        $('#nes-venue-id').on('change',function(){
            var venue_id = $(this).val();
            var data = {
                'action': 'nes_ajax_get_venue_locations',
                'venue_id': venue_id
            };
            $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){
                $('#nes-locations').html(response); 
                $('#nes-location-id').chosen({
                    placeholder_text_multiple: "Select some <?php echo $this->nes_settings['location_plural']; ?>",
                    width: "50%"
                });
            }); 
        });

        // toggle sections on click
        $('.nes-section-title').on('click', function(){
            $('i', this).toggleClass('bt-angle-up bt-angle-down');
            $(this).next().slideToggle('fast');
        });

        // toggle frequency on edit type toggle
        $('input[name="nes_edit_type"]').on('change',function(){
            if($(this).val() == 'one'){
                $('.nes-frequency-type').hide();
                $('.nes-recurrence').hide();
                $('.nes-one-dialog').dialog({                    
                                        width:400,
                                        modal:true,
                                        buttons: [{
                                            text: "OK",
                                            click: function(){
                                                $(this).dialog("close");
                                            }
                                        }]
                                    });
            }else if($(this).val() == 'upcoming'){
                $('.nes-frequency-type').fadeIn('fast');
                $('.nes-recurrence').fadeIn('fast');
                $('.nes-upcoming-dialog').dialog({                                        
                                        width:400,
                                        modal:true,
                                        buttons: [{
                                            text: "OK",
                                            click: function(){
                                                $(this).dialog("close");
                                            }
                                        }]
                                    });
            }else{
                $('.nes-frequency-type').fadeIn('fast');
                $('.nes-recurrence').fadeIn('fast');
                $('.nes-all-dialog').dialog({                                        
                                        width:400,
                                        modal:true,
                                        buttons: [{
                                            text: "OK",
                                            click: function(){
                                                $(this).dialog("close");
                                            }
                                        }]
                                    });
            }
        });

        // toggle basic fields on page load 
        if($('input[name="nes_event_type"]:checked').val() == 'onsite'){
            $('.nes-onsite-venue, #nes-locations, .nes-setup-time, .nes-cleanup-time, .nes-check-availability').fadeIn('fast');
            $('.nes-offsite-venue').hide();
        }

        // toggle basic fields on change
        $('input[name="nes_event_type"]').on('change',function(){
            if($(this).val() == 'offsite'){
                $('.nes-onsite-venue, #nes-locations, .nes-setup-time, .nes-cleanup-time, .nes-check-availability').hide();
                $('.nes-offsite-venue').fadeIn('fast');
            }else{
                $('.nes-onsite-venue, #nes-locations, .nes-setup-time, .nes-cleanup-time, .nes-check-availability').fadeIn('fast');
                $('.nes-offsite-venue').hide();
            }
        });   

        // toggle recurrence on page load 
        if($('input[name="nes_frequency_type"]:checked').val() != 'one'){
            $('.nes-recurrence').fadeIn('fast');
        }     

        // toggle recurrence on change
        $('input[name="nes_frequency_type"]').on('change',function(){
            if($(this).val() != 'one'){
                $('.nes-recurrence').fadeIn('fast');
            }else{
                $('.nes-recurrence').hide();
            }
        });        

        // toggle frequency on page load
        if($('input[name="nes_frequency_type"]:checked').val() == 'one'){
            $('.nes-series-start-date, .nes-frequency-end-date').hide();
        }else if($('input[name="nes_frequency_type"]:checked').val() == 'daily'){
            $('.nes-series-start-date, .nes-frequency-end-date').fadeIn('fast');
        }else if($('input[name="nes_frequency_type"]:checked').val() == 'weekly'){
            $('.nes-frequency-weekly').fadeIn('fast');
            $('.nes-series-start-date, .nes-frequency-end-date').fadeIn('fast');
        }else if($('input[name="nes_frequency_type"]:checked').val() == 'monthly'){
            $('.nes-frequency-weekly').fadeIn('fast');
            $('.nes-frequency-monthly').fadeIn('fast');
            $('.nes-series-start-date, .nes-frequency-end-date').fadeIn('fast');

            if($('select[name=nes_frequency_monthly]').val()){
                $('.nes-frequency-weekly').fadeIn('fast');
            }else{
                $('.nes-frequency-weekly').hide();
            }
        }else if($('input[name="nes_frequency_type"]:checked').val() == 'custom'){
            $('.nes-custom-datepicker-wrap').fadeIn('fast');
        }

        // toggle frequency on change
        $('input[name="nes_frequency_type"]').on('change',function(){
            if($(this).val() == 'one'){
                $('.nes-series-start-date, .nes-frequency-end-date').hide();
                $('.nes-frequency-weekly').hide();
                $('.nes-frequency-monthly').hide();
                $('.nes-custom-datepicker-wrap').hide();
            }else if($(this).val() == 'daily'){
                $('.nes-series-start-date, .nes-frequency-end-date').fadeIn('fast');
                $('.nes-frequency-weekly').hide();
                $('.nes-frequency-monthly').hide();
                $('.nes-custom-datepicker-wrap').hide();
            }else if($(this).val() == 'weekly'){
                $('.nes-series-start-date, .nes-frequency-end-date').fadeIn('fast');
                $('.nes-frequency-weekly').fadeIn('fast');
                $('.nes-frequency-monthly').hide();
                $('.nes-custom-datepicker-wrap').hide();
            }else if($(this).val() == 'monthly'){
                $('.nes-series-start-date, .nes-frequency-end-date').fadeIn('fast');
                $('.nes-frequency-weekly').fadeIn('fast');
                $('.nes-frequency-monthly').fadeIn('fast');
                $('.nes-custom-datepicker-wrap').hide();

                if($('select[name=nes_frequency_monthly]').val()){
                    $('.nes-frequency-weekly').fadeIn('fast');
                }else{
                    $('.nes-frequency-weekly').hide();
                }
            }else if($('input[name="nes_frequency_type"]:checked').val() == 'custom'){
                $('.nes-series-start-date, .nes-frequency-end-date').hide();
                $('.nes-frequency-weekly').hide();
                $('.nes-frequency-monthly').hide();
                $('.nes-custom-datepicker-wrap').fadeIn('fast');
            }
        });  

        // toggle weekly if monthly is empty
        $('select[name=nes_frequency_monthly]').on('change',function(){
            if($(this).val()){
                $('.nes-frequency-weekly').fadeIn('fast');
            }else{
                $('.nes-frequency-weekly').hide();
            }
        });

        // toggle tickets on page load 
        if($('input[name="nes_ticketed_event"]:checked').val() == 'no'){
            $('.nes-ticket-info').hide();
        }else{
            $('.nes-ticket-info').fadeIn('fast');
        }
        // toggle tickets on change
        $('input[name="nes_ticketed_event"]').on('change',function(){
            if($(this).val() == 'no'){
                $('.nes-ticket-info').hide();
            }else{
                $('.nes-ticket-info').fadeIn('fast');
            }
        });

        // ajax call to populate form fields for selected gravity form
        // $('.nes-gravity-forms').on('change',function(){
        //     var form_id = $(this).val();
        //     var data = {
        //         'action': 'nes_ajax_get_gravity_form_fields',
        //         'form_id': form_id,
        //         'post_id': <?php echo $post_id; ?>
        //     };
        //     $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){
        //         $('.nes-gf-form-fields').html(response); 
        //     }); 
        // }).change();        

        // ajax call to check availability
        $('.nes-check-availability-button').on('click',function(e){
            e.preventDefault();
            $('i', this).show();
            
            // get weekly checkbox values into array
            var weekly_vals = new Array();
            $("input[name='nes_frequency_weekly[]']:checked").each(function(){
                weekly_vals.push($(this).val());
            });

            // build postdata to send
            var data = {
                'action': 'nes_ajax_check_availability',
                'venue': $('select[name=nes_venue_id]').val(),
                'locations': $('#nes-location-id').val(),
                'date': $('input[name=nes_event_date]').val(),
                'custom_dates': $('input[name="nes_custom_date"]').val(),
                'setup_time': $('select[name=nes_start_setup_time]').val(),
                'start_time': $('select[name=nes_start_time]').val(),
                'end_time': $('select[name=nes_end_time]').val(),
                'cleanup_time': $('select[name=nes_end_cleanup_time]').val(),
                'frequency_type': $('input[name=nes_frequency_type]:checked').val(),
                'frequency_end_date': $('input[name=nes_frequency_end_date]').val(),
                'frequency_weekly': weekly_vals,
                'frequency_monthly': $('select[name=nes_frequency_monthly]').val(),
                'post_id': "<?php echo $post_id; ?>"
            };
            $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){
                $('.nes-availability-wrap').html(response); 
                $('.nes-check-availability-button i').hide();
                $('.nes-check-availability-button').removeClass('nes-required-field');
            }); 
        });    

        // add new ticket row
        $(document).on('click','.nes-add-remove-ticket .bt-plus-circle',function(){
            // no more than 5 tickets
            if($('.nes-add-remove-ticket').length < 10){
                $('.nes-tickets tbody').append('<tr><td class="nes-ticket-type"><select name="nes_ticket_type[]"><option value="standard">Standard</option><option value="user-defined">User Defined</option></select></td><td class="nes-ticket-description"><input type="text" name="nes_ticket_description[]" value="" /></td><td class="nes-ticket-price"><input type="number" name="nes_ticket_price[]" min="0" step="any" value="" /></td><td class="nes-ticket-suggested"><input type="number" name="nes_ticket_suggested[]" min="0" step="any" value="" disabled="disabled" /></td><td class="nes-add-remove-ticket"><i class="bts bt-plus-circle"></i><i class="bts bt-minus-circle"></i></td></tr>');
            }
        });            

        // remove ticket row
        $(document).on('click','.nes-add-remove-ticket .bt-minus-circle',function(){
            $(this).parents('tr').fadeOut('fast',function(){
                $(this).remove();
            });
        });          

        // update "readonly" for suggested pricing
        $(document).on('change','.nes-tickets select',function(){
            if($(this).val() == 'user-defined'){
                $(this).parent('.nes-ticket-type').siblings('.nes-ticket-suggested').children('input').attr('disabled', false);
            }else{
                $(this).parent('.nes-ticket-type').siblings('.nes-ticket-suggested').children('input').attr('disabled', true);
            }
        }); 


        ////////// VALIDATION //////////////////

        var validate = false;
        $(document).on('change', 'select[name="nes_venue_id[]"], select[name="nes_location_id[]"], #nes-event-datepicker, input[name="nes_custom_date"], select[name="nes_start_time"], select[name="nes_end_time"], input[name="nes_frequency_type"], select[name="nes_need_setup_time"], select[name="nes_need_cleanup_time"], #nes-frequency-end-date, select[name="nes_frequency_monthly"], input[name="nes_frequency_weekly[]"]',function(){
            validate = true;
        });

        $(document).on('click', '#publish', function(e){

            if(validate){

                // clear previous errors
                $('.nes-error-list').empty();
                $('.nes-required-field').each(function(){
                    $(this).removeClass('nes-required-field');
                });

                // check for missing fields
                var errors = new Array();

                // event details
                if($('input[name=nes_event_type]:checked').val() == 'offsite'){
                    if(!$('input[name=nes_offsite_venue_name]').val()){
                        $('input[name=nes_offsite_venue_name]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                        errors.push('Offsite Venue Name');
                    }
                    if(!$('input[name=nes_offsite_venue_address]').val()){
                        $('input[name=nes_offsite_venue_address]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                        errors.push('Offsite Venue Address');
                    }
                }else{
                    if(!$('#nes-location-id').val()){
                        $('#nes-location-id').next('.chosen-container').children('.chosen-choices').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                        errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' ' + "<?php echo $this->nes_settings['location_plural']; ?>");
                    }
                }

                // event date/time
                if(!$('input[name=nes_event_date]').val()){
                    $('#nes-event-datepicker').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                    errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' Date');
                }            
                if(!$('select[name=nes_start_time]').val()){
                    $('select[name=nes_start_time]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                    errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' Start Time');
                }
                if(!$('select[name=nes_end_time]').val()){
                    $('select[name=nes_end_time]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                    errors.push("<?php echo $this->nes_settings['event_single']; ?>" + ' End Time');
                }

                var setup = $('select[name=nes_start_setup_time]').val();
                var start = $('select[name=nes_start_time]').val();
                var end = $('select[name=nes_end_time]').val();
                var cleanup = $('select[name=nes_end_cleanup_time]').val();

                if($('input[name=nes_event_type]:checked').val() != 'offsite'){
                    if(setup.length && setup > start){
                            $('select[name=nes_start_setup_time]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                            errors.push('Conflicting Setup Time');
                    }
                }
                if(start >= end){
                    $('select[name=nes_start_time]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                    errors.push('Conflicting Start Time');
                }            
                if(end <= start){
                    $('select[name=nes_end_time]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                    errors.push('Conflicting End Time');
                }       
                if($('input[name=nes_event_type]:checked').val() != 'offsite'){         
                    if(cleanup.length && end > cleanup){
                        $('select[name=nes_end_cleanup_time]').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                        errors.push('Conflicting Cleanup Time');
                    }            
                }

                // recurring event fields
                if($('input[name=nes_edit_type]:checked').val() != 'one'){
                    if($('input[name=nes_frequency_type]:checked').val() != 'one' && $('input[name=nes_frequency_type]:checked').val() != 'custom'){           
                        if(!$('input[name=nes_frequency_end_date]').val()){
                            $('#nes-frequency-end-datepicker').addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                            errors.push('Series End Date');
                        }
                    }
                }

                // event tickets
                if($('input[name=nes_ticketed_event]:checked').val() == 'yes'){

                    $('.nes-ticket-description input').each(function(){
                        if(!$(this).val()){
                            $(this).addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                            errors.push('Ticket Description');
                        }
                    });                

                    $('.nes-ticket-price input').each(function(){
                        if(!$(this).val()){
                            $(this).addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                            errors.push('Ticket/Minimum Price');
                        }
                    });                

                    $('.nes-ticket-suggested input').each(function(){
                        if(typeof $(this).attr('readonly') === 'undefined'){
                            if(!$(this).val()){
                                $(this).addClass('nes-required-field').parents('.nes-meta-section').slideDown('fast');
                                errors.push('Ticket Suggested Price');
                            }
                        }
                    });
                }

                // check if onsite and available before submitting
                if($('input[name="nes_event_type"]:checked').val() == 'onsite'){
                    
                    // only allow if 
                    if($('.nes-availability-result li').hasClass('nes-stop-new')){
                        $('#nes-event-datepicker').addClass('nes-required-field');
                        errors.push('Event Date is unavailable');
                    }

                    // check if availability
                    if(!$('.nes-availability-result').length){
                        $('.nes-check-availability-button').addClass('nes-required-field');
                        errors.push('Check availability before submitting onsite events');
                    }
                }

                


                // generate validation error popup
                if(errors.length > 0){
                    e.preventDefault();
                    $.each(errors, function(key, value){
                        $('.nes-error-list').append('<li>'+ value + '</li>');
                    });
                    $('.nes-missing-fields').dialog({
                        width:400,
                        modal:true,
                        buttons: [{
                            text: "OK",
                            click: function(){
                                $(this).dialog("close");
                            }
                        }]
                    });
                }
            }
        });
    });
</script>