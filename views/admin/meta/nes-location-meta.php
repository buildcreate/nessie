<?php global $post; ?>
<?php $location = $post; ?>
<?php wp_nonce_field('save_location_meta', 'location_meta_nonce'); ?>
<div>
    <div id="nes-fields">
        <div class="nes-message">
            <h2>IMPORTANT: Deleting this will remove all <?php echo $this->nes_settings['venue_single']; ?> and <?php echo $this->nes_settings['event_single']; ?> associations with this <?php echo $this->nes_settings['location_single']; ?>. This CANNOT be undone!</h2>
        </div>
        <?php $venues = $this->nes_get_venues(); ?>
        <?php if($venues) : ?>
            <label><?php printf(__('Which %1$s does this %2$s belong to','nes'),$this->nes_settings['venue_single'],$this->nes_settings['location_single']); ?>?</label>
            <select id="nes-venue-id" name="nes_venue_id">
            <?php foreach($venues as $venue) : ?>
                <option value="<?php echo $venue->ID; ?>" 
                    <?php 
                        if($venue_id = get_post_meta($location->ID, 'nes_venue_id', true)){
                            if($venue_id == $venue->ID){echo 'selected';}
                        }else if(get_option('nes_default_venue') == $venue->ID){echo 'selected';} 
                    ?>
                ><?php echo $venue->post_title; ?></option>

            <?php endforeach; ?>
            </select>
        <?php endif; ?>
        <p id="nes-venue-availability">
            <input type="radio" id="nes-venue-availability-1" name="nes_venue_availability" value="venue" <?php if((get_post_meta($location->ID, 'nes_venue_availability', true) == "venue") || (get_post_meta($location->ID, 'nes_venue_availability', true) == "")){echo 'checked';} ?> />
            <label for="nes-venue-availability-1"><?php printf(__('Use %1$s availability','nes'),$this->nes_settings['venue_plural']); ?></label><br/>
            <input type="radio" id="nes-venue-availability-2" name="nes_venue_availability" value="custom" <?php if(get_post_meta($location->ID, 'nes_venue_availability', true) == "custom"){echo 'checked';} ?> />
            <label for="nes-venue-availability-2"><?php printf(__('Use custom availability for this %1$s','nes'),$this->nes_settings['location_single']); ?>. <em><?php printf(__('Leave times blank to make this %1$s unavailable on those days','nes'),$this->nes_settings['location_single']); ?>)</em></label>
        </p>
        <div class="nes-availability" style="display:none;"> 
            <?php $days = array(__('Monday','nes'), __('Tuesday','nes'), __('Wednesday','nes'), __('Thursday','nes'), __('Friday','nes'), __('Saturday','nes'), __('Sunday','nes')); ?>
            <?php foreach($days as $day) : ?>
            <div>
                <h4><?php echo $day; ?></h4>
                <label><?php _e('Start','nes'); ?></label><br/>
                <?php echo $this->nes_get_time_select('nes_location_'.strtolower($day).'_start', get_post_meta($location->ID, 'nes_location_'. strtolower($day). '_start', true)); ?><br/>
                <label><?php _e('End','nes'); ?></label><br/>
                <?php echo $this->nes_get_time_select('nes_location_'.strtolower($day).'_end', get_post_meta($location->ID, 'nes_location_'. strtolower($day). '_end', true)); ?>
            </div>  
            <?php endforeach; ?> 
            <span class="nes-clearer"></span> 
            <br/>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#nes-venue-id').chosen({
            placeholder_text_single: "<?php printf(__('Select a %1$s','nes'),$this->nes_settings['venue_single']); ?>"
        });  
        if($('#nes-venue-availability-2').prop('checked')){
            $('.nes-availability').show();
        }
        $('#nes-venue-availability').on('change', function(){
            $('.nes-availability').slideToggle('fast');
        });
    });
</script>