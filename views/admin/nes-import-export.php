<div class="nes-main-wrap">
	<h1 class="nes-page-title"><i class="btr bt-download"></i> Import/Export</h1>
	<hr/>
	<div>
		<h3>Export Nessie <?php echo $this->nes_settings['event_plural'];?></h3>
		<form method="post" action="" >
			<p>
				<label><?php echo $this->nes_settings['venue_single']; ?></label>
				<?php $venues = $this->nes_get_venues(); ?>
				<?php if($venues) : ?>
					<select id="nes-venue-id" name="nes_default_venue">
						<?php foreach($venues as $venue) : ?>
							<option value="<?php echo $venue->ID; ?>" <?php if(get_option('nes_default_venue') == $venue->ID){echo 'selected';} ?>><?php echo $venue->post_title; ?></option>
						<?php endforeach; ?>
							<option value="offsite">Offsite</option>
					</select>
				<?php else : ?>
					<p>No <?php echo $this->nes_settings['venue_plural']; ?> have been created yet. <a href="/wp-admin/post-new.php?post_type=nes_venue">Create new <?php echo $this->nes_settings['venue_single']; ?></a>
				<?php endif; ?>
			</p>
			<p>
				<label>From Date</label>
				<input class="datepicker" type="text" name="nes_from_date" />
			</p>
			<p>
				<label>To Date</label>
				<input class="datepicker" type="text" name="nes_to_date" />
			</p>
	
			<?php wp_nonce_field('nes_export_schedule','nes_export_schedule_nonce', true, true); ?>
			<button class="button" name="nes_export_button"><i class="btr bt-download"></i> Download CSV File</button>
		</form>
		<script type="text/javascript">
            jQuery(document).ready(function($){
	           // jquery datepicker
                $('.datepicker').datepicker({
                    dateFormat: "mm/dd/yy",
                    inline: true,
                    showOtherMonths: true,
                    showOn: "both", 
                    buttonText: "<i class='btr bt-calendar'></i>",
                });
            });
        </script>
        <br/>
	</div>

	<hr/>
	<div>
		<h3>Convert existing Vacancy <?php echo $this->nes_settings['event_plural']; ?> to Nessie</h3>
		<?php // check if ECP is active ?>
		<?php if(post_type_exists('va_reservation')) : ?>
			<?php 			
				// give an indicator of how many more events need to be imported
				global $wpdb;

				// get all vacancy reservations
				$query_string = "
	                SELECT p.ID
	                FROM $wpdb->posts AS p
	                WHERE p.post_type = 'va_reservation'
	                AND p.post_status = 'publish'
	                AND NOT EXISTS (
	                    SELECT p2.ID
	                    FROM $wpdb->posts AS p2
	                    WHERE p2.post_type = 'nes_event'
	                    AND (p.post_name = p2.post_name AND p2.post_name IS NOT NULL)
	                    AND p2.post_status IN ('publish','past_events')
	                )
	            ";
				$va_reservations = $wpdb->get_results($query_string, OBJECT);
			?>
			<p><em>There are <span class="nes-remaining-events"><?php echo count($va_reservations); ?></span> Vacancy <?php echo $this->nes_settings['event_plural']; ?> are available for conversion.</em></p>
			<form method="post" action="">
				<p><button class="button" name="nes_import_vacancy" value="1"><i class="btr bt-sign-in"></i> Convert Vacancy Events</button></p>
				<div style="display:none;" class="nes-message nes-vacancy">
					<p><strong>Conversion in progress. This can take quite a long time, please do not refresh the page.</strong> <i class="bts bt-spinner bt-pulse"></i></p>
					<ul class="nes-vacancy-result">
						<li>Events Converted: <span class="nes-vacancy-events">0</span></li>
						<li>Locations Converted: <span class="nes-vacancy-locations">0</span></li>
						<li>Venues Converted: <span class="nes-vacancy-venues">0</span></li>
					</ul>
					<br/>
					<p class="button nes-stop-import">stop conversion</p>
				</div>
			</form>
			<br/>
		<?php else : ?>
			<h4><em>Vacancy plugin needs to be active to import reservations.</em></h4>
		<?php endif; ?>
	</div>	

	<hr/>
	<div>
		<h3>Convert existing Events Calendar Pro events to Nessie</h3>
		<?php // check if ECP is active ?>
		<?php if(post_type_exists('tribe_events')) : ?>
			<?php 			
				// give an indicator of how many more events need to be imported
				global $wpdb;
	           	$query_string = "
					SELECT p.ID
					FROM $wpdb->posts AS p
					WHERE p.post_type = 'tribe_events'
					AND p.post_status = 'publish'
					AND NOT EXISTS (
						SELECT p2.ID                  
						FROM $wpdb->posts AS p2
						WHERE (p2.post_type = 'nes_event' OR p2.post_type = 'va_reservation')
						AND (p.post_name = p2.post_name	AND p2.post_name IS NOT NULL)
						AND p2.post_status IN ('publish','past_events')
					) 
				";
	            $ecp_events = $wpdb->get_results($query_string, OBJECT);
			?>
			<p><em>There are <span class="nes-remaining-ecp"><?php echo count($ecp_events); ?></span> Events Calendar Pro events are available for conversion.</em></p>
			<form method="post" action="">
				<p><button class="button" name="nes_import_ecp" value="1"><i class="btr bt-sign-in"></i> Convert Events Calendar Pro Events</button></p>
				<div style="display:none;" class="nes-message nes-ecp">
					<p><strong>Conversion in progress. This can take quite a long time, please do not refresh the page.</strong> <i class="bts bt-spinner bt-pulse"></i></p>
					<ul class="nes-ecp-result">
						<li>Events Converted: <span class="nes-ecp-events">0</span></li>
					</ul>
					<br/>
					<p class="button nes-stop-import">stop conversion</p>
				</div>
			</form>
			<br/>
		<?php else : ?>
			<h4><em>The Events Calendar plugin needs to be active to import events.</em></h4>
		<?php endif; ?>
	</div>	

	<hr/>
	<div>
		<h3>Import <?php echo $this->nes_settings['event_plural']; ?> from iCal file (.ics)</h3>
		<form id="nes-ical-import-form" method="post" action="" enctype="multipart/form-data">
			<p class="nes-ical-file"><input id="nes-ical-file" type="file" name="nes_ical_file" /> <i class="bts bt-spinner bt-pulse" style="display:none;"></i></p>
			<p class="nes-import-ical-button" style="display:none;"><button class="button" name="nes_import_ical" value="1"><i class="btr bt-upload"></i> Import iCal Events</button></p>
			<div style="display:none;" class="nes-message nes-ical">
				<p><strong>Import in progress. This can take quite a long time, please do not refresh the page.</strong> <i class="bts bt-spinner bt-pulse"></i></p>
				<ul class="nes-ical-result">
					<li>iCal Events Remaining: <span class="nes-ical-events-found">0</span></li>
					<li>iCal Events Imported: <span class="nes-ical-events-imported">0</span></li>
					<li>iCal Existing Events Skipped: <span class="nes-ical-existing-events">0</span></li>
				</ul>
				<br/>
				<p class="button nes-stop-import">stop import</p>
			</div>
		</form>
		<br/>
	</div>

	<script type="text/javascript">
		jQuery(document).ready(function($){

			///////////// VACANCY /////////////

			// show "importing" message
			$('button[name="nes_import_vacancy"]').on('click',function(){
				$(this).hide();
				$(this).parent('p').next('.nes-message').show();
			});

			// trigger the import
			$('button[name="nes_import_vacancy"]').on('click', function(e){
				e.preventDefault();
				nes_do_vacancy_import();
			});

			// set recursive import function to true
			var vacancy_go_flag = true;
			$('.nes-stop-import').on('click', function(){
				vacancy_go_flag = false;
			});

			function nes_do_vacancy_import(){
                var data = {
                    'action' : 'nes_ajax_vacancy_import'
                };

                $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){
                	// parse response
                	var res = $.parseJSON(response);

					// update event/location/venue tally
					var events = parseInt($('.nes-vacancy-events').text()) + parseInt(res.events);
                	$('.nes-vacancy-events').text(events);					
                	var locations = parseInt($('.nes-vacancy-locations').text()) + parseInt(res.locations);
                	$('.nes-vacancy-locations').text(locations);                	
                	var venues = parseInt($('.nes-vacancy-venues').text()) + parseInt(res.venues);
                	$('.nes-vacancy-venues').text(venues);

                	// update remaining events tally
                	var remaining = parseInt($('.nes-remaining-events').text()) - parseInt(res.events);
                	$('.nes-remaining-events').text(remaining);

                	// check if we need to repeat
                	if(res.again && vacancy_go_flag){
                		nes_do_vacancy_import();
                	}else{
                		// all done!
                		$('.nes-vacancy').html('All done!');
                	}
                });
			}			

			////////////// ECP //////////////

			// show "importing" message
			$('button[name="nes_import_ecp"]').on('click',function(){
				$(this).hide();
				$(this).parent('p').next('.nes-message').show();
			});

			// trigger the import
			$('button[name="nes_import_ecp"]').on('click', function(e){
				e.preventDefault();
				nes_do_ecp_import();
			});

			// set recursive import function to true
			var ecp_go_flag = true;
			$('.nes-stop-import').on('click', function(){
				ecp_go_flag = false;
			});

			function nes_do_ecp_import(){
                var data = {
                    'action' : 'nes_ajax_ecp_import'
                };

                $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){
                	// parse response
                	var res = $.parseJSON(response);

					// update event tally
					var events = parseInt($('.nes-ecp-events').text()) + parseInt(res.events);
                	$('.nes-ecp-events').text(events);					            	

                	// update remaining events tally
                	var remaining = parseInt($('.nes-remaining-ecp').text()) - parseInt(res.events);
                	$('.nes-remaining-ecp').text(remaining);

                	// check if we need to repeat
                	if(res.again && ecp_go_flag){
                		nes_do_ecp_import();
                	}else{
                		// all done!
                		$('.nes-ecp').html('All done!');
                	}
                });
			}			

			////////////// iCal //////////////

			var ical_url;

			// Set an event listener on the Choose File field.
			$('input[name="nes_ical_file"]').bind("change", function(e){

				// show loading
				$('.nes-ical-file i').show();

				var formData = new FormData();
				formData.append("action", "upload-attachment");

				var fileInputElement = document.getElementById("nes-ical-file");
				formData.append("async-upload", fileInputElement.files[0]);
				formData.append("name", fileInputElement.files[0].name);
					
				//also available on page from _wpPluploadSettings.defaults.multipart_params._wpnonce
				<?php $my_nonce = wp_create_nonce('media-form'); ?>
				formData.append("_wpnonce", "<?php echo $my_nonce; ?>");

				$.ajax({
                	url : "/wp-admin/async-upload.php",
                	type : "POST",
                	data : formData,
                	cache: false,
                	processData: false,
					contentType: false,
                	success : function(response){
               			// parse response
	                	var res = $.parseJSON(response);
	                	ical_url = res.data.url;
                		
                		$('.nes-ical-file i').hide();
                		$('.nes-import-ical-button').fadeIn('fast');
	                }
                });
			});

			// show "importing" message
			$('button[name="nes_import_ical"]').on('click',function(){
				$(this).hide();
				$(this).parent('p').next('.nes-message').show();
			});

			// trigger the import
			$('.nes-import-ical-button').on('click', function(e){
				e.preventDefault();
				nes_do_ical_import(0);
			});

			// set recursive import function to true
			var ical_go_flag = true;
			$('.nes-stop-import').on('click', function(){
				ical_go_flag = false;
			});

			function nes_do_ical_import(current_event_count){

                var data = {
                    'action' : 'nes_ajax_ical_import',
                    'ical_url' : ical_url,
                    'current_event_count' : current_event_count
                };              

                $.post("<?php echo NES_AJAX_HANDLER; ?>", data, function(response){
                	// parse response
                	var res = $.parseJSON(response);

console.log(res);

					// update tallys
					var events_imported = parseInt($('.nes-ical-events-imported').text()) + parseInt(res.event_imported);
                	$('.nes-ical-events-imported').text(events_imported);							

                	var ical_existing_events = parseInt($('.nes-ical-existing-events').text()) + parseInt(res.event_skipped);
                	$('.nes-ical-existing-events').text(ical_existing_events);		

                	var events_remaining = parseInt(res.events_found) - events_imported - ical_existing_events;
                	$('.nes-ical-events-found').text(events_remaining);	 			            	

                	// check if we need to repeat
                	if(res.again && ical_go_flag){
                		nes_do_ical_import(res.current_event_count);
                	}else{
                		// all done!
                		$('.nes-ical').html('All done!');
                	}
                });
			}
		});

	</script>
</div>