=== Nessie ===
Contributors: kraftpress, buildcreate, a2rocklobster
Tags: events, reservation system, reservation scheduling, reservation, appointment system, appointment scheduling, appointment, user submitted, calendar, booking, scheduling, schedule, venue, location, vacancy
Requires at least: 3.5.0
Tested up to: 4.1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A full featured Event handling solution

== Description ==

The perfect booking solution for any person or establishment that needs to allow users to submit reservation/appointment requests. Create your venues and locations, add the [nessie] shortcode in your page or post, and you are all setup and ready to go. Lots of customizable options with an easy to use frontend and backend interfaces.

= Features =

* Add venues, like offices, stores, etc
* Add locations within your venues, like conference rooms, staff lounges, salon chairs, restaurant tables etc
* One simple shortcode to use Nessie on any page or post
* Customizable labels for your Venues/Locations/Events
* Custom reservation availability on a per location basis per day basis
* Event Conflict detection
* Sell Event tickets with Gravity forms
* Recurring Events

== Installation ==

1. Upload 'nessie' to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Add new Venues under the Nessie nav item in your WordPress admin
4. Add new Locations under the Nessie nav item in y our WordPress admin
5. Place the shortcode [nessie] in your post or page
6. Users can now start submitting reservation requests, it's that easy!

== Changelog ==

= 1.7 =
* Add "Blackout date/time" capability

= 1.6 =
* Fixed "check availability" bug
* Fixed "unexpected output" on activation
* Added "follow down page" option for the "tabbed layout"
* Added header info to be compatible with "github-updater"
* Removed old updater code

= 1.5 =
* Added "auto population" of user profile data for ticket forms
* Added "redirect back to event" to login link

= 1.4 =
* Fixed "single event template" showing in other places than just the single page
* Fixed activation error

= 1.3 =
* Fixed pagination on "first and last event"
* Fixed pagination order of "next" and "previous", specifically for events on the same day

= 1.2 =
* Fixed incorrect "attendees" X of Y tickets sold message
* Added iCal feed title option
* Added clarifying heading to custom notification Header/Footer settings

= 1.1.1 =
* Fixed "default venue" ID
* Fixed iCal import from hanging

= 1.1 =
* Updated Offsite names and single event "where" to only be shown when not blank

= 1.0.9.1 =
* Fixed an error when there is a gravity form with no fields

= 1.0.9 =
* Removed all $_SESSION usage to prevent potential server configuration conflicts
* Updated some css "button styles" on reservation calendar

= 1.0.8 =
* Fixed "hierarchical" custom post type memory issue
* Fixed admin column filtering and sorting

= 1.0.7 =
* Updated "import" queries to match the event counts on the options page

= 1.0.6 =
* More "undefined index" error bug fixes
* Fixed widget categories error

= 1.0.5 =
* Fixed "undefined index" errors
* Fixed "check availability" issues when conflicting with themselves/series
* Fixed custom excerpt function for events calendar tooltip

= 1.0.4 =
* Added admin dashboard widgets
* Added event status filter
* Fixed "Standard" reservation view

= 1.0.3 =
* Updated the "check updates" interval

= 1.0.2 =
* Added auto updater
* Fixed various form submission errors
* Fixed "upcoming events" widget

= 1.0.1 = 
* Added 'required' to nessie tickets credit card field
* Fixed logged in/out visiblity issues
* Removed bullet point styling from 'phone' field in when making reservations
* Set plugin to deactivate if Gravity Forms is not installed

= 1.0 =
* Initial release